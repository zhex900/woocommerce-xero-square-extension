<?php
/**
 * Xero Refund Manager class.
 *
 * @package WC_Xero_Square_Extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
require_once ABSPATH. '/wp-content/plugins/woocommerce-xero-square-extension/includes/requests/class-wc-xr-request-actions.php' ;
require_once ABSPATH. '/wp-content/plugins/woocommerce-xero-square-extension/includes/requests/class-wc-xr-request-void-invoice.php' ;
require_once ABSPATH. '/wp-content/plugins/woocommerce-xero-square-extension/includes/requests/class-wc-xr-request-void-payment.php' ;
require_once ABSPATH. '/wp-content/plugins/woocommerce-xero-square-extension/includes/requests/class-wc-xr-request-manual-journal.php' ;
require_once ABSPATH. '/wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-manual-journal.php' ;
require_once ABSPATH. '/wp-content/plugins/woocommerce-gateway-stripe/includes/class-wc-stripe-helper.php';

//const SQUARE_FEE = 1.9;

/**
 * Xero Refund Manager.
 */
class WC_XR_Refund_Manager extends  WC_XR_Credit_Card_Fee_Manager {

	/**
	 * Xero settings.
	 *
	 * @var WC_XR_Settings
	 */
	private $settings;

    /**
     * @var WC_XR_Logger
     */
	private $logger;

	/**
	 * WC_XR_Payment_Manager constructor.
	 *
	 * @param WC_XR_Settings $settings Xero settings.
	 */
	public function __construct( WC_XR_Settings $settings ) {
        parent::__construct( $settings );
		$this->settings = $settings;
		$this->setup_hooks();
        $this->logger = new WC_XR_Logger( $this->settings );
	}

	/**
	 * Set up callbacks to the hooks.
	 */
	public function setup_hooks() {
		remove_action( 'woocommerce_order_fully_refunded', array( $this, 'maybe_void_invoice' ) );
	    add_action( 'woocommerce_order_status_refunded', array($this,'send_refund'), 20, 1 );
	}

	/**
	 * Send refund to the XERO API.
     *
     * Void invoice
     * Void payment
     * Void credit card fee journal entry
	 *
     * No refund for payment method cod (pay via invoice) with paid status
     *
	 * @param int $order_id Order ID.
     * @param int $refund_id Refund ID.
	 *
	 * @return bool
	 */
    public function send_refund( $order_id)
    {

        if (!$order_id) {
            return false;
        }
        // Getting an instance of the order object
        $order = wc_get_order($order_id);
        $payment_method = $order->get_payment_method();

        // only can process full refund.
        $refund_amount = $order->get_total_refunded();
        if ($refund_amount != $order->get_total()) {
            $order->add_order_note(__('Xero cannot send partial refunds. Refund amount: $' . $refund_amount, 'wc-xero'));
            return false;
        }

	    if ( $order->get_transaction_id() !== '' ) {
		    // void payment must be first
		    $this->void_payment( $order );
	    }

        $this->void_invoice($order);

        // none cash payments should have credit card fees
        if (strtolower($payment_method) != 'cod' && strpos(strtolower($payment_method), 'cash') === false){
            $this->void_credit_card_fee($order);
        }
    }

    /**
     * @param WC_Order $order Order object.
     *
     * @return bool
     */
    public function void_payment( $order ) {

        $payment_id =$order->get_meta('_xero_payment_id');

	    if ( $payment_id === '' ) {
		    $order->add_order_note( __( 'Xero cannot void payment. Payment id does not exist.', 'wc-xero' ) );
		    return false;
	    }

        $request = new WC_XR_Request_Void_Payment( $this->settings, $payment_id );

        return WC_XR_Request_Actions::send(
            $order,
            $request,
            $this->logger,
            function (WC_Order $order, $xml_response) {
                $order->add_order_note(sprintf(
                /* translators: Payment ID from Xero. */
                    __('Xero Payment voided. Transaction ID: %s', 'wc-xero'),
                    (string)$xml_response->Id
                ));
            });
    }

    /**
     * @param WC_Order $order Order object.
     *
     * @return bool
     */
    public function void_invoice( $order ) {

        $invoice_id = $order->get_meta('_xero_invoice_id');

        if (!$invoice_id) {
            $order->add_order_note(__('Xero cannot void invoice. Invoice id is not found.', 'wc-xero'));
            return false;
        }

        $request = new WC_XR_Request_Void_Invoice( $this->settings, $invoice_id );

        return WC_XR_Request_Actions::send(
            $order,
            $request,
            $this->logger,
            function (WC_Order $order, $xml_response) {
                $order->add_order_note(sprintf(
                /* translators: Payment ID from Xero. */
                    __('Xero Invoice voided. Transaction ID: %s', 'wc-xero'),
                    (string)$xml_response->Id
                ));
                // chanage xero invoice status to void
                update_post_meta($order->get_id(),'_xero_invoice_status',  "VOIDED");
            });
    }

    /**
     * @param WC_Order $order      Order object.
     *
     * Credit card fees are manual journals
     * To void manual journals is done by a reversing journal
     * @return bool
     */
    public function void_credit_card_fee( $order )
    {
        // Bank transaction Request.
        $manual_journal_request = new WC_XR_Request_ManualJournal($this->settings, $this->get_refund_credit_card_fee_by_order($order));

        return WC_XR_Request_Actions::send(
            $order,
            $manual_journal_request,
            $this->logger,
            function (WC_Order $order, $xml_response) {
                $manual_journal_id = (string)$xml_response->Id;
                $order->update_meta_data('_xero_manual_journal_refund_credit_card_fee_id', $manual_journal_id);
                $order->save_meta_data();
                $order->add_order_note(sprintf(
                /* translators: Payment ID from Xero. */
                    __('Xero refund credit card fee created. Transaction ID: %s', 'wc-xero'),
                    (string)$xml_response->Id
                ));
            });
    }

    /**
     * Reverse/refund Credit Card Fee by order.
     *
     * @param WC_Order $order Order object.
     * @param string $invoice_number
     *
     * @return WC_XR_ManualJournal
     */
    public function get_refund_credit_card_fee_by_order( $order, $invoice_number='' ) {

        if( $invoice_number == '') {
            $invoice_number = $order->get_meta('_xero_invoice_number');
        }

        $order_date =  date( 'Y-m-d H:i:s' );
        $date_parts = explode( ' ', $order_date );
        $order_ymd = $date_parts[0];

        // The Payment object.
        $manual_journal = new WC_XR_ManualJournal();

        // Get payment gateway specific account codes
        list ( $clearing_account, $fee_account ) = $this->get_account_codes($order);

        // Credit credit card fee account.
        $manual_journal->set_credit_account_code( $fee_account  );

        // Debit clearing account.
        $manual_journal->set_debit_account_code( $clearing_account );

        // Set the payment date.
        $manual_journal->set_date( apply_filters( 'woocommerce_xero_order_payment_date', $order_ymd, $order ) );

        // Set the amount.
        $manual_journal->set_amount( $this->get_credit_card_fee($order) );

        // Set narration/description
        $manual_journal->set_narration( "Reverse/refund credit card fees for invoice: {$invoice_number}");

        return $manual_journal;
    }
}
