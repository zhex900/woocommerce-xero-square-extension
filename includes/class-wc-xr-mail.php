<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Send notification email after payment
 */
class WC_XR_Mail {
	public function __construct() {
		add_action( 'woocommerce_email_before_order_table', [ $this, 'failed_payment_message' ], 10, 4 );
		/**
		 * Turn off emails
		 **/
		// untested code
		// remove_action( 'woocommerce_product_on_backorder_notification', array( $this, 'backorder' ), 10 );
	}

	public function failed_payment_message( WC_Order $order, $sent_to_admin, $plain_text, $email ) {
		$payment_error = get_post_meta( $order->get_id(), '_payment_error', true );
		if ( ! empty( $payment_error ) ) {

			$order->add_order_note( __( 'Payment failed: '.$payment_error['message']. ' '.$payment_error['charge'], 'woocommerce-xero-square-extension' ),
				false, true );

			echo '<p><h3 style="color: tomato; text-transform: uppercase;">Payment failure. ' .
			     '<span> ' . $payment_error['message'] . '</span>' .
				 '</h3><h4>' .
				 'Please update your credit card details. This order requires manual payment.' .
				 '</h4></p>';
		}
	}

	public static function send_invoice_to_customer( $order_id ) {
		if ( ! $order_id ) {
			return;
		}

		$order = wc_get_order( $order_id );

		do_action( 'xero_extension_before_invoice_to_customer', $order_id );

		WC()->mailer()->customer_invoice( $order );

		do_action( 'xero_extension_after_send_invoice_to_customer', $order_id );

		// Note the event.
		$order->add_order_note( __( 'Order details and invoice sent to customer.', 'woocommerce-xero-square-extension' ),
			false, true );

	}
}
