<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Square_Update_Customer extends WC_XR_Request_Square {

	public function __construct($customer_id,$customer_data) {
		$this->set_method( 'PUT' );
		$this->set_endpoint( 'customers/'.$customer_id);
		$this->set_post_fields(
		    json_encode(
                $customer_data
            )
        );
	}
}
