<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

class WC_XR_Request_Sync_Account extends WC_XR_Request_Extension {

    public function __construct( WC_XR_Settings $settings, WC_XR_Account $account ) {
        parent::__construct( $settings );
        $this->set_method( 'PUT' );
        // Set Endpoint
        $this->set_endpoint( 'Accounts');
        // Set the XML
        $this->set_body($account->to_xml());
    }
}