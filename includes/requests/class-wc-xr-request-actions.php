<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class WC_XR_Request_Actions
{
    /**
     * @param WC_Order $order Order object.
     * @param WC_XR_Request_Extension $request
     * @param WC_XR_Logger $logger
     * @param callable $request_handler
     *
     * @return bool
     */
    public static function send(
        WC_Order $order,
        WC_XR_Request_Extension $request,
        WC_XR_Logger $logger,
        callable $request_handler)
    {

        $request_name = strtoupper(get_class($request));
        // Try to do the request.
        try {
            // Logging start.
            $logger->write("START XERO NEW {$request_name}. order_id: {$order->get_id()}");

            // Do the request.
            $request->do_request();

            // Parse XML Response.
            $xml_response = $request->get_response_body_xml();

            // Check response status.
            if ('OK' === (string)$xml_response->Status) {
                // Do something with request response
                $request_handler($order, $xml_response);
                // Write logger.
                $logger->write('XERO RESPONSE:' . "\n" . $request->get_response_body());
            } else {
                // Logger write.
                $logger->write('XERO ERROR RESPONSE:' . "\n" . $request->get_response_body());

                // Error order note.
                $error_num = (string)$xml_response->ErrorNumber;
                $error_msg = (string)$xml_response->Elements->DataContractBase->ValidationErrors->ValidationError->Message;
                $order->add_order_note(sprintf(
                    __('ERROR creating Xero %s. ErrorNumber: %1$s | Error Message: %2$s', 'wc-xero'),
                    $request_name,
                    $error_num,
                    $error_msg
                ));
            }
        } catch (Exception $e) {
            // Add Exception as order note.
            $order->add_order_note($e->getMessage());

            $logger->write($e->getMessage());

            return false;
        }

        // Logging end.
        $logger->write("END XERO {$request_name}");

        return true;
    }

    /**
     * @param WC_XR_Request_Extension $request
     * @param WC_XR_Logger $logger
     * @param callable $request_handler
     *
     * @return bool
     */
    public static function send_(
        WC_XR_Request_Extension $request,
        WC_XR_Logger $logger,
        callable $request_handler)
    {

        $request_name = strtoupper(get_class($request));
        // Try to do the request.
        try {
            // Logging start.
            $logger->write("START XERO NEW {$request_name}");

            // Do the request.
            $request->do_request();

            // Parse XML Response.
            $xml_response = $request->get_response_body_xml();

            // Check response status.
            if ('OK' === (string)$xml_response->Status) {
                // Write logger.
                $logger->write('XERO RESPONSE:' . "\n" . $request->get_response_body());
                // Do something with request response
                return $request_handler($xml_response);
            } else {
                // Logger write.
                $logger->write('XERO ERROR RESPONSE:' . "\n" . $request->get_response_body());
                return false;
            }
        } catch (Exception $e) {
            // Add Exception as order note.
            $logger->write($e->getMessage());
            return false;
        }
    }
}
