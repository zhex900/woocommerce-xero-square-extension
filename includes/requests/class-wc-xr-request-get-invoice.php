<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Get_Invoice extends WC_XR_Request_Extension {

	public function __construct( WC_XR_Settings $settings, $invoice_id ) {
		parent::__construct( $settings );

		$this->set_method( 'GET' );
		$this->set_endpoint( 'Invoices/'.$invoice_id );
	}
}
