<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Get_Customer extends WC_XR_Request_Extension {

	public function __construct( WC_XR_Settings $settings, $page_number ) {
		parent::__construct( $settings );

		$this->set_method( 'GET' );
		$this->set_endpoint( 'Contacts');
        $this->set_query( array(
            'where' => 'EmailAddress!=null&&EmailAddress!=""&&IsCustomer=true',
            'page'=>$page_number
        ) );
	}
}
