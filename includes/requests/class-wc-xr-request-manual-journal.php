<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_ManualJournal extends WC_XR_Request_Extension {

	public function __construct( WC_XR_Settings $settings, WC_XR_ManualJournal $manualJournal ) {
        $settings = apply_filters( 'woocommerce_xero_manual_journal_request_settings', $settings, $manualJournal );
        parent::__construct( $settings );

        // Set Endpoint
        $this->set_endpoint( 'manualjournals' );

        // Set the XML
        $this->set_body( '<ManualJournal>' . $manualJournal->to_xml() .'</ManualJournal>' );
	}
}
