<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Square_Get_Payments extends WC_XR_Request_Square {

    public function __construct($begin_time, $end_time, $location_id = null)
    {
        if (is_null($location_id)) {
            $location_id = $this->get_location_id();
        }
		$this->set_method( 'GET' );
		$this->set_api_version('v1');
        file_put_contents('/tmp/logs.log', 'location: ' . $location_id . PHP_EOL, FILE_APPEND);
        $this->set_endpoint($location_id . "/payments?begin_time=" . $begin_time . "&end_time=" . $end_time);
	}
}
