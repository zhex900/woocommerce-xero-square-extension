<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Square {

	/**
	 * @var String API URL
	 */
	const API_URL = "https://connect.squareup.com";
    private $api_version = 'v2';
	/**
	 * The request endpoint
	 *
	 * @var String
	 */
	private $endpoint = '';

    /**
     * The request post fields
     *
     * @var String JSON encoded
     */
	private $post_fields;

	/**
	 * The request method
	 *
	 * @var string
	 */
	private $method = 'PUT';

	/**
	 * The request response
	 * @var string
	 */
	private $response = null;

	/**
	 * WC_XR_Request_Square constructor.
	 */
	public function __construct() {
	}

	/**
	 * Method to set endpoint
	 *
	 * @param $endpoint
	 */
	protected function set_endpoint( $endpoint ) {
		$this->endpoint = $endpoint;
	}
    protected function set_api_version( $version){
	    $this->api_version = $version;
    }

	/**
	 * Get the endpoint
	 *
	 * @return String
	 */
	protected function get_endpoint() {
		return $this->endpoint;
	}

	/**
	 * @return string
	 */
	protected function get_method() {
		return $this->method;
	}

	/**
	 * @param string $method
	 */
	protected function set_method( $method ) {
		$this->method = $method;
	}

	/**
	 * Get response
	 *
	 * @return string
	 */
	public function get_response() {
		return $this->response;
	}

	/**
	 * Return the response body in JSON object
	 *
	 */
	public function get_response_json() {
		if ( ! is_null( $this->response ) ) {
			return json_decode( $this->response);
		}

		return null;
	}

	/**
	 * Clear the response
	 *
	 * @return bool
	 */
	private function clear_response() {
		$this->response = null;

		return true;
	}

	private function get_post_fields(){
	    return $this->post_fields;
    }

    public function set_post_fields($post_fields){
        $this->post_fields=$post_fields;
    }

    public function get_location_id(){
	    $settings = get_option('woocommerce_squareconnect_settings');
	    return $settings['location'];
    }
	/**
	 * Do the request
	 * 		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
     *      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	 * @return string
	 */
	public function do_request() {
        $curl = curl_init();

        $options= array(
            CURLOPT_URL => self::API_URL.'/'.$this->api_version.'/'.$this->get_endpoint(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST=> FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->get_method(),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                "authorization: Bearer " . get_option('wc_xero_square_access_token'),
                "cache-control: no-cache",
            ),
        );

        if( $this->post_fields!=null) {
            $options[CURLOPT_POSTFIELDS]=$this->get_post_fields();
        }
        curl_setopt_array($curl, $options);

        $this->response = curl_exec($curl);



        error_log("[response] [" . date("Y-m-d H:i:s") . "] " .get_option('woocommerce_square_merchant_access_token'). print_r($this->response, true) . "\n", 3, '/tmp/logs.log');

        $err = curl_error($curl);

        curl_close($curl);

        return $err;
	}
}
