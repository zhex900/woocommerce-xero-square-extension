<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Square_Get_Customer extends WC_XR_Request_Square {

	public function __construct($customer_id='') {
	    if($customer_id!=''){
	        $customer_id='/'.$customer_id;
        }
		$this->set_method( 'GET' );
		$this->set_endpoint( 'customers'.$customer_id);
	}
}
