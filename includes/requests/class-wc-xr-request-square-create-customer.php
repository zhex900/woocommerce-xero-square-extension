<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Square_Create_Customer extends WC_XR_Request_Square {

	public function __construct(array $customer_data) {
		$this->set_method( 'POST' );
		$this->set_endpoint( 'customers');
		$this->set_post_fields(
		    json_encode(
                $customer_data
            )
        );
	}
}
