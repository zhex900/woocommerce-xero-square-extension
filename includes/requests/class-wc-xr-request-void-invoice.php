<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Void_Invoice extends WC_XR_Request_Extension {

	public function __construct( WC_XR_Settings $settings, $invoice_id ) {
		parent::__construct( $settings );

		$this->set_method( 'POST' );
		$this->set_endpoint( 'Invoices/'.$invoice_id );

        // Set the XML
        $this->set_body(
            '<Invoice>
                      <InvoiceID>'.$invoice_id.'</InvoiceID>
                      <Status>VOIDED</Status>
                   </Invoice>');
	}
}
