<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Square_Get_Inventory_Changes extends WC_XR_Request_Square {

    public function __construct($catalog_object_id)
    {
		$this->set_method( 'GET' );
		$this->set_api_version('v2');
        $this->set_endpoint( "inventory/$catalog_object_id/changes");
	}
}
