<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class WC_XR_Request_Square_Get_Location_Details extends WC_XR_Request_Square
{

    public function __construct()
    {
        $this->set_method('GET');
        $this->set_endpoint("locations");
    }
}
