<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

class WC_XR_Request_Void_Payment extends WC_XR_Request_Extension {

    public function __construct( WC_XR_Settings $settings, $payment_id ) {
        $settings = apply_filters( 'woocommerce_xero_void_payment_request_settings', $settings, $payment_id );
        parent::__construct( $settings );
        $this->set_method( 'POST' );
        // Set Endpoint
        $this->set_endpoint( 'Payments/'.$payment_id );

        // Set the XML
        $this->set_body(
            '<Payments>
                        <Payment>
                            <Status>DELETED</Status>
                        </Payment>
                   </Payments>' );

    }

}