<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

class WC_XR_Request_Sync_Product extends WC_XR_Request_Extension {

    public function __construct( WC_XR_Settings $settings, WC_XR_Product $product ) {
        parent::__construct( $settings );
        $this->set_method( 'POST' );
        // Set Endpoint
        $this->set_endpoint( 'Items');
        // Set the XML
        $this->set_body($product->to_xml());
    }
}