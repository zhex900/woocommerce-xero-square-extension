<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Request_Square_Get_Transactions extends WC_XR_Request_Square {

    public function __construct($transactions_id, $location_id)
    {
		$this->set_method( 'GET' );
        $this->set_api_version('v2');
        $this->set_endpoint("locations/" . $location_id . "/transactions/{$transactions_id}");
	}
}
