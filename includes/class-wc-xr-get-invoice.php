<?php

namespace Xero_Extension;

use ActionScheduler;
use ActionScheduler_Store;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Get_Invoice
{
    function __construct()
    {
        $this->setup_hooks();
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('wp_ajax_get_invoice', array($this, 'get_invoice_ajax'));
    }

    /**
     * Load and generate the template output with ajax
     */
    public function get_invoice_ajax()
    {
        // Check the nonce
        if (empty($_GET['action']) || !check_admin_referer($_GET['action'])) {
//            wp_die(__('You do not have sufficient permissions to access this page.', 'woocommerce-pdf-invoices-packing-slips'));
        }

        // Check if all parameters are set
        if (empty($_GET['order_id'])) {
            wp_die(__("There is order id for this order", 'woocommerce-pdf-invoices-packing-slips'));
        }

        $order_ids = (array)array_map('absint', explode('x', $_GET['order_ids']));
        // Process oldest first: reverse $order_ids array
        $order_ids = array_reverse($order_ids);

        // set default is allowed
        $allowed = true;

        // check if user is logged in
        if (!is_user_logged_in()) {
            $allowed = false;
        }

        // Check the user privileges
        if (!(current_user_can('manage_woocommerce_orders') || current_user_can('edit_shop_orders')) && !isset($_GET['my-account'])) {
            $allowed = false;
        }

        // User call from my-account page
        if (!current_user_can('manage_options') && isset($_GET['my-account'])) {

            // Check if current user is owner of order IMPORTANT!!!
            if (!current_user_can('view_order', $order_ids[0])) {
                $allowed = false;
            }
        }

        $allowed = apply_filters('WC_Xero_Square_Extension_check_privs', $allowed, $order_ids);

        if (!$allowed) {
//            wp_die(__('You do not have sufficient permissions to access this page.', 'woocommerce-pdf-invoices-packing-slips'));
        }

        // if we got here, we're safe to go!
        try {
            // Generate the output
            list ($invoice, $invoice_number,) = $this->get_xero_invoice($_GET['order_id']); //$invoice_number,$_GET['xero_invoice_id']);

            if (strlen($invoice) != 0) {
                $this->pdf_headers("$invoice_number.pdf", 'download');
                echo $invoice;
            } else {
                wp_die(sprintf(__("Error in getting Xero invoice for the selected order. Please try again.",
                    'woocommerce-pdf-invoices-packing-slips'), ''));
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        exit;
    }

    static public function get_user_upload_path()
    {
        $current_user = wp_get_current_user();
        $upload_dir = wp_upload_dir();

        if (0 != $current_user->ID && isset($current_user->user_login) && !empty($upload_dir['basedir'])) {
            return $upload_dir['basedir'] . '/users/' . $current_user->user_login;
        } else {
            return $upload_dir['basedir'] . '/users/guest';
        }
    }

    static public function get_xero_invoice_id_by_order_id($order_id)
    {
        $order = wc_get_order($order_id);
        return $order->get_meta('_xero_invoice_id');
    }

    static public function get_xero_invoice($order_id, $force_download = false) //$invoice_number,$xero_invoice_id)
    {
        $xero_invoice_id = self::get_xero_invoice_id_by_order_id($order_id);
        $invoice_number = self::get_xero_invoice_number_by_order_id($order_id, $xero_invoice_id);
        $upload_path = self::get_user_upload_path();
        $invoice_path = $upload_path . '/invoices/' . $invoice_number . '.pdf';
        $order = wc_get_order($order_id);
        $payment_method = $order->get_payment_method();

        if (file_exists($invoice_path) && !$force_download && $payment_method != 'cod') {
            return array(file_get_contents($invoice_path), $invoice_number, $invoice_path);
        } else {
            $settings = new \WC_XR_Settings();
            $invoice_request = new \WC_XR_Request_Get_Invoice($settings, $xero_invoice_id);
            try {
                $invoice_request->do_request(array('Accept' => 'application/pdf'));
            } catch (\Exception $exception) {
                // this to catch Request failed due OAuth error: rate limit exceeded
                return ['', '', ''];
            }
            $response = $invoice_request->get_response();

            if ($upload_path != '' && strlen($response['body']) > 0) {
                file_put_contents($invoice_path, $response['body']);
            }
            return array($response['body'], $invoice_number, $invoice_path);
        }
    }

    static public function xero_pending_action_exists()
    {
        $pending = ActionScheduler::store()->query_actions([
            'status' => ActionScheduler_Store::STATUS_PENDING,
            'group' => 'xero'
        ]);
        $running = ActionScheduler::store()->query_actions([
            'status' => ActionScheduler_Store::STATUS_RUNNING,
            'group' => 'xero'
        ]);

        return (count($pending) + count($running)) > 0;
    }

    // what if order id or invoice id don't have value or cannot get object
    static public function get_xero_invoice_number_by_order_id($order_id, $xero_invoice_id)
    {
        $order = wc_get_order($order_id);
        $payment_method = $order->get_payment_method();
        $default_invoice_number = '';

        if ($order->meta_exists('_xero_invoice_id')) {
            $default_invoice_number = "INVW-$order_id";
        }

        if ($xero_invoice_id == '') {
            return $default_invoice_number;
        }

        if ($payment_method == 'cod'
            || !$order->meta_exists('_xero_invoice_number')
            || !$order->meta_exists('_xero_invoice_status')
            || $order->get_meta('_xero_invoice_status') == 'AUTHORISED'
            || strlen($order->get_meta('_xero_invoice_number')) < 2) {

            if (self::xero_pending_action_exists()) {
                if (!$order->meta_exists('_xero_invoice_number')) {
                    return $default_invoice_number;
                } else {
                    return $order->get_meta('_xero_invoice_number');
                }
            }

            list($invoice_number, $invoice_status) = self::get_xero_invoice_number_and_status($xero_invoice_id);

            if ($invoice_number != '' && $invoice_status != '') {
                $order->update_meta_data('_xero_invoice_number', $invoice_number);
                $order->update_meta_data('_xero_invoice_status', $invoice_status);
                $order->save_meta_data();
            }
        }

        return $order->get_meta('_xero_invoice_number');
    }

    static public function get_xero_invoice_number_and_status($xero_invoice_id)
    {
        $settings = new \WC_XR_Settings();
        $invoice_request = new \WC_XR_Request_Get_Invoice($settings, $xero_invoice_id);
        try {
            $invoice_request->do_request(array('Accept' => 'application/json'));
        } catch (\Exception $exception) {
            // this to catch Request failed due OAuth error: rate limit exceeded
            return ['', ''];
        }
        $response = $invoice_request->get_response();
        $invoice_json = json_decode($response['body'])->Invoices[0];
        return array($invoice_json->InvoiceNumber, $invoice_json->Status);
    }

    private function pdf_headers($filename, $mode = 'inline', $pdf = null)
    {
        switch ($mode) {
            case 'download':
                header('Content-Description: File Transfer');
                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment; filename="' . $filename . '"');
                header('Content-Transfer-Encoding: binary');
                header('Connection: Keep-Alive');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                break;
            case 'inline':
            default:
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $filename . '"');
                break;
        }
    }
}
