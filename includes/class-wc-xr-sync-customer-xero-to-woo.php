<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/requests/class-wc-xr-request-get-customer.php';

class WC_XR_Sync_Customer_Xero_To_Woo extends WC_XR_Woo_Customer
{
    const DEFAULT_ROLE ='member';

    function __construct(WC_XR_Settings $settings)
    {
        parent::__construct($settings);
        $this->setup_hooks();

        $this->new_customers=0;
        $this->existing_customers=0;
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('wp_ajax_sync_xero_to_woo_customer', array($this, 'sync_xero_to_woo_customer_ajax'));
        add_action('woocommerce_email_footer', array($this, 'add_user_pass_to_email'),10,1);
    }

    /**
     *
     * This is to add the user password in the new account email.
     * When the user register through the website, the generated password is already
     * in the email. But when the user is created through Xero to Woo Sync. The
     * generated password is not included. This is a workaround.
     *
     * @param WC_Email_Customer_New_Account $email
     */
    public function add_user_pass_to_email($email)
    {
        if ('customer_new_account' == $email->id &&
            'yes' === get_option( 'woocommerce_registration_generate_password' ) &&
            ""==$email->password_generated) {

            printf(esc_html__('Your password has been automatically generated: %s', 'woocommerce'), '<strong>' . esc_html($email->user_pass) . '</strong>');

        }
    }

    public function sync_xero_to_woo_customer_ajax()
    {
        $customers = $this->get_all_customers();

        echo 'Sync Xero Customers to WooCommerce ';
        echo PHP_EOL."-----------------------------------------------" . PHP_EOL.PHP_EOL;
        foreach ($customers as $customer) {
//            echo print_r($customer).PHP_EOL;
            $this->sync_woo_customer($customer);
        }

        $number_of_customers = sizeof($customers);
        echo PHP_EOL.PHP_EOL."-----------------------------------------------" . PHP_EOL.PHP_EOL;
        echo "Created new customers: {$this->new_customers}" . PHP_EOL;
        echo "Updated customers: {$this->existing_customers}" . PHP_EOL;
        echo "Total Xero customers: {$number_of_customers}" . PHP_EOL;
        die;
    }

    private function get_all_customers()
    {
        $customers = array();

        for ($i = 1; ; $i++) {
            $customers_per_page = $this->get_customers_by_page($i);
            if (is_null($customers_per_page) || sizeof($customers_per_page) == 0) {
                break;
            }
            $customers = array_merge($customers, $customers_per_page);
        }
        return $customers;
    }

    private function get_customers_by_page($page_number)
    {
        $request = new \WC_XR_Request_Get_Customer($this->settings, $page_number);
        $contacts = null;
        try {
            // Logging start.
            $this->logger->write("START XERO NEW get_customers_by_page: {$page_number}");

            $request->do_request(array('Accept' => 'application/json'));
            $response = json_decode($request->get_response()['body']);
            if ($response->Status == 'OK') {
                $contacts = $response->Contacts;
            } else {
                $this->logger->write('XERO ERROR RESPONSE:' . "\n" . print_r($response));
            }
        } catch (Exception $e) {

            $this->logger->write($e->getMessage());
        }

        // Logging end.
        $this->logger->write("END XERO get_customers_by_page");

        return $contacts;
    }

    private function sync_woo_customer($xero_customer)
    {

        $woo_customer_data = $this->transform_customer_data($xero_customer);

//        echo print_r($xero_customer).PHP_EOL;
//        echo print_r($woo_customer_data).PHP_EOL;
        $woo_customer = get_users(array('xero_contact_id' => $xero_customer->ContactID));
        if ($woo_customer!==false &&
            !$woo_customer = get_user_by('email', $xero_customer->EmailAddress)) {
            echo 'Created: '. $woo_customer_data['nickname']. ' -> ';
            $woo_customer = $this->create_woo_customer($woo_customer_data);
            echo $woo_customer instanceof WP_User . PHP_EOL;
        } else {
            echo 'Updated: '. $woo_customer_data['nickname']. PHP_EOL;
            unset($woo_customer_data['username']);
            unset($woo_customer_data['password']);
            self::update_woo_customer($woo_customer, $woo_customer_data);
            $this->existing_customers++;
        }
    }
//
//    /**
//     * Create a customer.
//     */
//    public function create_woo_customer($customer_data)
//    {
//        // Sets the username.
//        $customer_data['username'] = !empty($customer_data['username']) ? $customer_data['username'] : '';
//        // Sets the password.
//        $customer_data['password'] = !empty($customer_data['password']) ? $customer_data['password'] : '';
//        // Attempts to create the new customer.
//        $customer_id = wc_create_new_customer($customer_data['email'], $customer_data['username'], $customer_data['password']);
//        // Checks for an error in the customer creation.
//        if (is_wp_error($customer_id)) {
////            throw new \Exception($customer_id->get_error_code(), $customer_id->get_error_message());
//            return 'FAILED -'. $customer_id->get_error_message();
//        }else {
//            $this->new_customers++;
//            // Added customer data.
//            $customer = get_user_by('id',$customer_id);
//            $customer->set_role(self::DEFAULT_ROLE);
//            self::update_woo_customer($customer_id, $customer_data);
//            return 'OK';
//        }
//    }

//    /**
//     * Add/Update customer data.
//     *
//     * @since 2.5.0
//     * @param int $customer_id The customer ID
//     * @param array $fields
//     */
//    public static function update_woo_customer($customer_id, $fields)
//    {
//        foreach ($fields as $key => $value){
//            if( isset($value) ) {
//                update_user_meta($customer_id, $key, wc_clean($value));
//            }
//        }
//    }

    private function transform_customer_data($xero_customer){
        $woo_customer=array();
        $woo_customer['nickname']=ucwords($xero_customer->Name);
        $woo_customer['first_name']=ucwords($this->get_firstname($xero_customer));
        $woo_customer['last_name']=ucwords($this->get_lastname($xero_customer));
        $woo_customer['username']=$this->get_username($xero_customer);
        $woo_customer['password']=wp_generate_password(10,false,false);
        $woo_customer['email']=strtolower($xero_customer->EmailAddress);
        $woo_customer['xero_contact_id']=$xero_customer->ContactID;
        // shipping
        $woo_customer = array_merge(
            $woo_customer,
            $this->get_address($woo_customer,$xero_customer,'shipping',1));
        // billing
        $woo_customer = array_merge(
            $woo_customer,
            $this->get_address($woo_customer,$xero_customer,'billing',0));

        return $woo_customer;
    }

//    private function get_username($xero_customer){
//        // use email prefix as username
//        preg_match('/^(.*)@.*$/', strtolower($xero_customer->EmailAddress), $output_array);
//        $username=$output_array[1];
//
//        /**
//         * Generate an unique username.
//         */
//        $tries = 0;
//        $num_tries=10;
//        while ( ( $exists = username_exists( $username ) ) && $tries++ < $num_tries ) {
//            $username = $username . '_' . mt_rand();
//        }
//
//        return $username;
//    }

    private function get_firstname($xero_customer){
        $name = explode(' ',$xero_customer->Name);
        if( isset($xero_customer->FirstName) ||$xero_customer->FirstName==''){
            return $name[0];
        }else{
            return $xero_customer->FirstName;
        }
    }

    private function get_lastname($xero_customer){
        $name = explode(' ',$xero_customer->Name);
        if( isset($xero_customer->LastName) ||$xero_customer->LastName==''){
            if( sizeof($name) >1 ){
                // remove the first element
                array_shift($name);
                return implode($name,' ');
            }else{
                return '';
            }
        }else{
            return $xero_customer->FirstName;
        }
    }

    private function get_address($woo_customer, $xero_customer,$address_prefix,$xero_address_index){

        // if no address exist return empty array
        if( !$this->xero_address_exist($xero_customer, 0)
            && !$this->xero_address_exist($xero_customer, 1)){
            return array();
        }

        if( !$this->xero_address_exist($xero_customer, $xero_address_index) ){
            if($xero_address_index==1){
                $xero_address_index=0;
            }else{
                $xero_address_index=1;
            }
            return $this->get_address($woo_customer, $xero_customer,$address_prefix,$xero_address_index);
        }

        $woo_address[$address_prefix.'_first_name']=$woo_customer['first_name'];
        $woo_address[$address_prefix.'_last_name']=$woo_customer['last_name'];
        $woo_address[$address_prefix.'_company']=$xero_customer->Name;
        $woo_address[$address_prefix.'_address_1']=ucwords($xero_customer->Addresses[$xero_address_index]->AddressLine1);
        $woo_address[$address_prefix.'_address_2']=ucwords($xero_customer->Addresses[$xero_address_index]->AddressLine2);
        $woo_address[$address_prefix.'_city']=strtoupper($xero_customer->Addresses[$xero_address_index]->City);
        $woo_address[$address_prefix.'_postcode']=$xero_customer->Addresses[$xero_address_index]->PostalCode;
        $woo_address[$address_prefix.'_country']=strtoupper($xero_customer->Addresses[$xero_address_index]->Country);
        $woo_address[$address_prefix.'_state']=strtoupper($xero_customer->Addresses[$xero_address_index]->Region);
        $woo_address[$address_prefix.'_phone']=$this->find_phone_number($xero_customer);
        $woo_address[$address_prefix.'_email']=$woo_customer['email'];
        return $woo_address;
    }

    private function xero_address_exist($xero_customer, $xero_address_index){
        return isset($xero_customer->Addresses[$xero_address_index]->AddressLine1) && trim($xero_customer->Addresses[$xero_address_index]->AddressLine1) != "";
    }

    private function find_phone_number($xero_customer)
    {
        $phone_number = $this->get_phone_number($xero_customer,'MOBILE');
        if ($phone_number == ''){
            return $this->get_phone_number($xero_customer,'DEFAULT');
        }else{
            return $phone_number;
        }
    }

    private function get_phone_number($xero_customer,$type){
        $phone_number = array_pop(array_filter($xero_customer->Phones, function ($phone) use ($type)  {
            return $phone->PhoneType==strtoupper($type);
        }));
        return $phone_number->PhoneCountryCode . "" . $phone_number->PhoneAreaCode ."". $phone_number->PhoneNumber;
    }
}
