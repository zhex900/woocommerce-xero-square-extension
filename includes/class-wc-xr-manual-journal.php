<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class WC_XR_ManualJournal
{

    private $debit_account_code = '';
    private $date = '';
    private $credit_account_code = '';
    private $amount = 0;
    private $narration = '';
    private $line_amount_type = 'Inclusive';
    private $debit_tax_type = "INPUT";
    private $credit_tax_type = "BASEXCLUDED";

    public function set_credit_tax_type($type)
    {
        $this->credit_tax_type = $type;
    }

    public function set_debit_tax_type($type)
    {
        $this->debit_tax_type = $type;
    }

    /**
     * @return string
     */
    public function get_line_amount_type()
    {
        return apply_filters('woocommerce_xero_journal_line_amount_type', $this->line_amount_type, $this);
    }

    /**
     * @param string $line_amount_type
     */
    public function set_line_amount_type($line_amount_type)
    {
        $this->line_amount_type = $line_amount_type;
    }

    /**
     * @return string
     */
    public function get_narration()
    {
        return apply_filters('woocommerce_xero_journal_narration', $this->narration, $this);
    }

    /**
     * @param string $narration
     */
    public function set_narration($narration)
    {
        $this->narration = $narration;
    }

    /**
     * @return string
     */
    public function get_debit_account_code()
    {
        return apply_filters('woocommerce_xero_journal_debit_account_code', $this->debit_account_code, $this);
    }

    /**
     * @param string $debit_account_code
     */
    public function set_debit_account_code($debit_account_code)
    {
        $this->debit_account_code = $debit_account_code;
    }

    /**
     * @return string
     */
    public function get_credit_account_code()
    {
        return apply_filters('woocommerce_xero_journal_credit_account_code', $this->credit_account_code, $this);
    }

    /**
     * @param string $credit_account_code
     */
    public function set_credit_account_code($credit_account_code)
    {
        $this->credit_account_code = $credit_account_code;
    }
    /**
     * @return string
     */
    public function get_date()
    {
        return apply_filters('woocommerce_xero_journal_date', $this->date, $this);
    }

    /**
     * @param string $date
     */
    public function set_date($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function get_amount()
    {
        return apply_filters('woocommerce_xero_journal_amount', $this->amount, $this);
    }

    /**
     * @param int $amount
     */
    public function set_amount($amount)
    {
        $this->amount = floatval($amount);
    }

    /**
     * Return XML of Manual Journal
     * Debits are positive, credits are negative value
     * @return string
     */
    public function to_xml()
    {

        $xml = '<Status>POSTED</Status>';
        $xml .= '<Narration>' . $this->get_narration() . '</Narration>';
        $xml .= '<LineAmountTypes>' . $this->get_line_amount_type() . '</LineAmountTypes>';
        $xml .= '<JournalLines>
                    <JournalLine>
                      <Description>' . $this->get_narration() . '</Description>
                      <LineAmount>' . $this->get_amount() . '</LineAmount>
                      <TaxType>' . $this->debit_tax_type . '</TaxType>
                      <AccountCode>' . $this->get_debit_account_code() . '</AccountCode>
                    </JournalLine>
                    <JournalLine>
                      <Description>' . $this->get_narration() . '</Description>
                      <LineAmount>-' . $this->get_amount() . '</LineAmount>
                      <TaxType>' . $this->credit_tax_type . '</TaxType>
                      <AccountCode>' . $this->get_credit_account_code() . '</AccountCode>
                    </JournalLine>
                 </JournalLines>';

        return $xml;
    }

}
