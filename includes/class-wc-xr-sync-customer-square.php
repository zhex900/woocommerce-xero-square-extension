<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Test scenarios
 *
 * Woo to Square (new customers) PASSED
 * Woo to Square (update customers) PASSED
 *
 * Square to Woo (new customers) PASSED
 * Square to Woo (update customers) PASSED
 *
 */

/**
 *
 * Bi-directional sync between Square and WooCommerce customers
 *
 * Class WC_XR_Sync_Customer_Square
 *
 */
class WC_XR_Sync_Customer_Square extends WC_XR_Woo_Customer
{
    const new = 'new';
    const EXISTING = 'existing';

    private $SUCCESS_OR_FAILED = [true => 'Success', false => 'Failed'];

    function __construct($settings)
    {
        parent::__construct($settings);
        $this->setup_hooks();
        $this->new_customers = 0;
        $this->existing_customers = 0;
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('wp_ajax_sync_square_to_woo_customer', [$this, 'sync_square_to_woo_customer_ajax']);
        add_action('wp_ajax_sync_woo_to_square_customer', [$this, 'sync_woo_to_square_customer_ajax']);
        
        // sync woo customer to square when a new woo customer is registered.
        add_action('user_register', [$this, 'sync_woo_to_square_customer']);

        // sync woo customer to square when an existing customer info is updated.
        add_action('profile_update', [$this, 'sync_woo_to_square_customer']);
        add_action("woocommerce_customer_save_address", [$this, 'sync_woo_to_square_customer']);
    }

    public function sync_square_to_woo_customer_ajax()
    {
        $request = new WC_XR_Request_Square_Get_Customer();
        $request->do_request();
        $response = $request->get_response_json();

        if (property_exists($response, 'errors')) {
            echo 'Error in retrieving Square customers.' . PHP_EOL;
            echo print_r($response);
            return false;
        } else {
            $this->pretty_print(
                'Square to WooCommerce',
                $this->sync_customers($response->customers));

            return true;
        }
    }

    public function pretty_print($message, array $result)
    {

        echo "Sync $message Customers";
        echo PHP_EOL . "-----------------------------------------------" . PHP_EOL;
        foreach ($result[self::new] as $item) {
            echo 'Created: ' . $item[0] . '->' . $this->SUCCESS_OR_FAILED[$item[1]] . PHP_EOL;
        }
        foreach ($result[self::EXISTING] as $item) {
            echo 'Updated: ' . $item[0] . '->' . $this->SUCCESS_OR_FAILED[$item[1]] . PHP_EOL;
        }
    }

    public function sync_woo_to_square_customer($user_id){
        $customer = get_user_by('id', $user_id);
        $square_customer = $this->find_customer_in_square($customer);

        if (is_null($square_customer)) {
//            echo 'Created: ' . $customer->nickname . ' -> ' .
//                $this->SUCCESS_OR_FAILED[$this->create_customer_in_square($customer)] . PHP_EOL;
            $this->create_customer_in_square($customer);
        } else {
//            echo 'Updated: ' . $customer->nickname . '-> ' .
//                $this->SUCCESS_OR_FAILED[self::sync_customer($square_customer, $customer)]
//                . PHP_EOL;
            self::sync_customer($square_customer, $customer);
            $this->existing_customers++;
        }
    }

    public function sync_woo_to_square_customer_ajax()
    {
        // get all woo customers with role saints
        $customers = get_users('role=member');

        echo 'Sync WooCommerce Customers to Square ';
        echo PHP_EOL . "-----------------------------------------------" . PHP_EOL;

        foreach ($customers as $customer) {
            $this->sync_woo_to_square_customer($customer->ID);
        }

        $number_of_customers = sizeof($customers);

        echo PHP_EOL . "-----------------------------------------------" . PHP_EOL;
        echo "Created new customers: {$this->new_customers}" . PHP_EOL;
        echo "Updated customers: {$this->existing_customers}" . PHP_EOL;
        echo "Total customers: {$number_of_customers}" . PHP_EOL;
        die;
    }

    public static function convert_to_square_data(WP_User $customer)
    {
        // request will fail if email is not valid
        // skip inserting state and country.
        $address_type = 'billing';
        $customer_data = [
            "reference_id" => "$customer->ID",
            "nickname" => ucwords(strtolower($customer->nickname)),
            "given_name" => ucwords(strtolower($customer->first_name)),
            "family_name" => ucwords(strtolower($customer->last_name)),
            "email_address" => strtolower($customer->user_email),
            "phone_number" => get_user_meta($customer->ID, $address_type . '_phone', true),
            "address" => [
                "address_line_1" => get_user_meta($customer->ID, $address_type . '_address_1', true),
                "address_line_2" => get_user_meta($customer->ID, $address_type . '_address_2', true),
                "locality" => get_user_meta($customer->ID, $address_type . '_city', true),
                "postal_code" => get_user_meta($customer->ID, $address_type . '_postcode', true),
            ],
        ];

        return $customer_data;
    }

    /**
     *
     * @param WP_User $woo_customer
     * @return object $square_customer
     */
    private function find_customer_in_square(WP_User $woo_customer)
    {
        $square_customer = null;
        $square_customer_id = get_user_meta($woo_customer->ID, '_square_customer_id', true);
        if (!$square_customer_id) {
            // search square for the same email address
            $email = $woo_customer->user_email;
            $request = new WC_XR_Request_Square_Get_Customer();
            $request->do_request();
            $response = $request->get_response_json();
            $find = array_filter($response->customers, function ($customer) use ($email) {
                return $customer->email_address == $email;
            });
            if (sizeof($find) != 0) {
                $square_customer = array_shift($find);
                update_user_meta($woo_customer->ID, '_square_customer_id', $square_customer->id);
            }
        } else {
            //check if the store square customer id exist
            $request = new WC_XR_Request_Square_Get_Customer($square_customer_id);
            $request->do_request();
            $response = $request->get_response_json();
            if (!property_exists($response, 'errors')) {
                $square_customer = $response->customer;
            }
        }
        return $square_customer;
    }

    private function create_customer_in_square(WP_User $customer)
    {
        $customer_data = self::convert_to_square_data($customer);

        $request = new WC_XR_Request_Square_Create_Customer($customer_data);
        $request->do_request();
        $response = $request->get_response_json();
        if (property_exists($response, 'errors')) {
//            @TODO use logger print_r($response);
            return false;
        } else {
            $this->new_customers++;
            update_user_meta($customer_data['reference_id'], '_square_customer_id', $response->customer->id);
            return true;
        }
    }

    public static function update_customer_in_square($square_customer_id, WP_User $customer)
    {
        $customer_data = self::convert_to_square_data($customer);

        // only update if the last update in square is greater than woo.
        $request = new WC_XR_Request_Square_Update_Customer($square_customer_id, $customer_data);
        $request->do_request();
        $response = $request->get_response_json();

        if (property_exists($response, 'errors')) {
            echo 'Error:' . PHP_EOL;
            echo print_r($response);
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * Find the corresponding woo customer by square customer
     * It also make sure square and woo customer details are
     * in sync.
     *
     * If the square customer does not exist in woo. A new
     * corresponding customer is created in woo.
     *
     * If the square customer exists in woo. Update both
     * woo and square. The most recent customer detail will overwrite
     * older customer detail.
     *
     * Find woo customer by the id saved in Square. If this exist, sync it
     * If the saved id doesn't exist, try to use the email address.
     *
     * @param object $square_customer
     * @return bool|WP_User
     */
    public static function get_woo_customer_by_square_customer($square_customer)
    {
        $woo_customer_id = $square_customer->reference_id;
        $woo_customer_by_id = get_user_by('id', $woo_customer_id);
        $woo_customer_by_email = get_user_by('email', $square_customer->email_address);

        file_put_contents('/tmp/logs.log', print_r($woo_customer_by_email, true) . PHP_EOL, FILE_APPEND);

        if ($woo_customer_id != '' && $woo_customer_by_id !== false) {
            self::sync_customer($square_customer, $woo_customer_by_id);
            return $woo_customer_by_id;
        } elseif ($woo_customer_by_email != false) {
            self::sync_customer($square_customer, $woo_customer_by_email);
            return $woo_customer_by_email;
        } else {
            file_put_contents('/tmp/logs.log', print_r('create new woo user', true) . PHP_EOL, FILE_APPEND);

            // create new woo customer
            $user = self::create_woo_customer(self::convert_to_woo_data($square_customer, true));
            file_put_contents('/tmp/logs.log', print_r($user, true) . PHP_EOL, FILE_APPEND);

            // assign user to member role
            $user->add_role('member');
            return $user;
        }
    }

    /**
     *
     * Sync customer details in Square and Woo. Xero don't need to be synced here.
     * Xero will not be used to update or create customer.
     * Woo customer updates will be synced to Xero at invoice generation.
     *
     * The most recent customer detail will overwrite
     * older customer detail.
     *
     * @param object $square_customer
     * @param WP_User $woo_customer
     *
     * @return bool. True means success and false means failed.
     *
     */
    public static function sync_customer($square_customer, WP_User $woo_customer)
    {
        $square_last_update = (new DateTime($square_customer->updated_at))->getTimestamp();
        $woo_last_update = get_user_meta($woo_customer->ID, 'last_update', true); // timestamp.

        // if woo is more recent
        if ($square_last_update < $woo_last_update) {
            //update square
            return self::update_customer_in_square(
                $square_customer->id,
                $woo_customer);
        } else { // square is more recent
            //update woo
            self::update_woo_customer($woo_customer->ID, self::convert_to_woo_data($square_customer));
            // check if square customer reference id is the correct woo customer id.
            return self::update_woo_id_in_square($square_customer->reference_id, $woo_customer->ID);
        }
    }

    public static function update_woo_id_in_square($square_customer_reference_id, $woo_customer_id)
    {
        // check if square customer reference id is the correct woo customer id.
        if ($square_customer_reference_id != $woo_customer_id) {
            $request = new WC_XR_Request_Square_Update_Customer($square_customer_reference_id, ["reference_id" => $woo_customer_id]);
            $error_message = $request->do_request();
            $request->get_response_json();
            return $error_message == ''; // no error message, means success.
        }
        return true;
    }

    /**
     *
     * Convert Square customer object into WooCommerce customer array.
     *
     * @param object $square_customer
     * @param bool $create_customer . If true, username and password will be created.
     * @return array
     *
     */
    public static function convert_to_woo_data($square_customer, $create_customer = false)
    {
        $square_customer = (array)$square_customer;
        $address_type = 'billing';
        $address_shipping = 'shipping';

        // key (woo) and value (square)
        $conversion = [
            "first_name" => "given_name",
            "last_name" => "family_name",
            "nickname" => "nickname",
            "email" => "email_address",
            $address_type . "_first_name" => "given_name",
            $address_type . "_last_name" => "family_name",
            $address_type . "_email" => "email_address",
            $address_type . "_phone" => "phone_number",
            $address_shipping . "_first_name" => "given_name",
            $address_shipping . "_last_name" => "family_name",
            $address_shipping . "_email" => "email_address",
            $address_shipping . "_phone" => "phone_number",
            $address_type . "_address_1" => "address_line_1",
            $address_type . "_address_2" => "address_line_2",
            $address_type . "_city" => "locality",
            $address_type . "_state" => "administrative_district_level_1",
            $address_type . "_postcode" => "postal_code",
            '_square_customer_id' => 'id',
        ];

        $woo_customer_data = [];

        if ($create_customer) {
            $woo_customer_data['username'] = self::get_username($square_customer['email_address']);
            $woo_customer_data['password'] = wp_generate_password(10, false, false);
        }
        foreach ($conversion as $woo_key => $square_key) {

            if (strpos($woo_key, $address_type) !== false) {
                if( array_key_exists('address', $square_customer)) {
                    if( property_exists($square_customer['address'], $square_key)) {
                        $woo_customer_data[$woo_key] = $square_customer['address']->$square_key;
                        $woo_customer_data[str_replace($address_type,$address_shipping,$woo_key)] = $square_customer['address']->$square_key;
                    }
                }
            } else {
                if( array_key_exists($square_key, $square_customer)) {
                    $woo_customer_data[$woo_key] = $square_customer[$square_key];
                }
            }
        }
        return $woo_customer_data;
    }

    /**
     *
     * Sync an array Square customer objects to WooCommerce.
     *
     * If the Square customer exists in Woo. Update it.
     * Otherwise create a new Woo customer and save the woo customer id
     * in Square customer reference id.
     *
     * returns an array of customers with result as true or false.
     *
     * @param array $square_customers
     * @return array $result
     */
    private function sync_customers(array $square_customers)
    {
        $result = [];
        foreach ($square_customers as $square_customer) {
            $woo_customer = $this->find_woo_customer($square_customer);
            if (!is_null($woo_customer)) {
                $result[self::EXISTING][] = [$square_customer->nickname, $this->sync_customer($square_customer, $woo_customer)];
            } else {
                $woo_customer_data = $this->convert_to_woo_data($square_customer, true);
                $woo_customer = $this->create_woo_customer($woo_customer_data);
                $result = self::update_woo_id_in_square($square_customer->reference_id, $woo_customer->ID);
                $result[self::new] = [$square_customer->nickname, $result];
            }
        }
        return $result;
    }

    /**
     *
     * Find the corresponding square customer in WooCommerce
     *
     * @param $square_customer
     * @return null|WP_User. Return null if nothing is found.
     *
     */
    private function find_woo_customer($square_customer)
    {
        // use square customer's reference id first
        $woo_customer_id = $square_customer->reference_id;
        $woo_customer = get_user_by('id', $woo_customer_id);
        if ($woo_customer_id != '' && $woo_customer !== false) {
            return $woo_customer;
        }

        // if there is no reference id, use email to look for it.
        $woo_customer = get_user_by('email', $square_customer->email_address);
        if ($woo_customer !== false) {
            return $woo_customer;
        }

        // nothing is found.
        return null;
    }
}
