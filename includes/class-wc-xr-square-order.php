<?php
/**
 * @package WC_Xero_Square_Extension
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

/**
 *
 */
class WC_XR_Square_Order
{

    private $stripe_payment_account;

    public function __construct()
    {
        $this->setup_hooks();
        $this->stripe_payment_account = get_option('wc_xero_payment_account');
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('woocommerce_admin_order_totals_after_total', array($this, 'display_order_fee'));
        add_action('woocommerce_admin_order_totals_after_total', array($this, 'display_order_payout'));
    }


    public function get_transaction_url($return_url, $order, $instance)
    {
        return $order->get_meta('square_payment_url');
    }



    /**
     * Displays the Square fee
     *
     * @since 4.1.0
     *
     * @param int $order_id
     */
    public function display_order_fee($order_id)
    {
        $order = wc_get_order($order_id);

        if ($order->get_payment_method() != 'square_pos_card') {
            return;
        }

        $fee = $order->get_meta('square_processing_fee_money');
        $currency = 'AUD';

        if (!$fee || !$currency) {
            return;
        }

        ?>

        <tr>
            <td class="label square-fee">
                <?php echo wc_help_tip(__('This represents the fee Square collects for the transaction.', 'woocommerce-gateway-square')); ?>
                <?php esc_html_e('Square Fee:', 'woocommerce-gateway-square'); ?>
            </td>
            <td width="1%"></td>
            <td class="total">
                -&nbsp;<?php echo wc_price($fee, array('currency' => $currency)); ?>
            </td>
        </tr>

        <?php
    }

    /**
     * Displays the net total of the transaction without the charges of Square.
     *
     * @since 4.1.0
     *
     * @param int $order_id
     */
    public function display_order_payout($order_id)
    {

        $order = wc_get_order($order_id);

        if ($order->get_payment_method() != 'square_pos_card') {
            return;
        }

        $net = floatval($order->get_total()) - floatval($order->get_meta('square_processing_fee_money'));
        $currency = 'AUD';

        if (!$net || !$currency) {
            return;
        }

        ?>

        <tr>
            <td class="label square-payout">
                <?php echo wc_help_tip(__('This represents the net total that will be credited to your Square bank account. This may be in the currency that is set in your Square account.', 'woocommerce-gateway-square')); ?>
                <?php esc_html_e('Square Payout:', 'woocommerce-gateway-square'); ?>
            </td>
            <td width="1%"></td>
            <td class="total">
                <?php echo wc_price($net, array('currency' => $currency)); ?>
            </td>
        </tr>

        <?php
    }
}
