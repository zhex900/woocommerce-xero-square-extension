<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Sync_Account_Code_Woo_To_Xero
{
    /**
     * Xero settings.
     *
     * @var WC_XR_Settings
     */
    private $settings;
    /**
     * @var WC_XR_Logger
     */
    private $logger;

    private $account_codes;

    function __construct(WC_XR_Settings $settings)
    {
        $this->settings = $settings;
        $this->logger = new WC_XR_Logger($this->settings);
        $this->setup_hooks();
        $this->account_codes =
            json_decode(
                file_get_contents(ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/xero-account-codes.json'),
                true);

    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('wp_ajax_sync_woo_to_xero_account_codes', [$this, 'sync_woo_to_xero_account_codes_ajax']);
    }

    /**
     *
     * Sync Accounts Codes
     */
    public function sync_woo_to_xero_account_codes_ajax()
    {
        foreach ($this->account_codes as $account_code) {
            $account = new WC_XR_Account($account_code);
            $request = new WC_XR_Request_Sync_Account($this->settings, $account);
            WC_XR_Request_Actions::send_(
                $request,
                $this->logger,
                static function () {
                });
        }
        echo 'Please check xero to verify the correct account is created.';
    }
}
