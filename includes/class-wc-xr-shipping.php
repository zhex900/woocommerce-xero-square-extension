<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Shipping
{

    function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_filter('woocommerce_checkout_fields', array($this, 'check_if_special_member_delivery'), 10, 2);
        add_action('woocommerce_after_checkout_form',  array($this,'shipping_toggle'),100 );
    }

    /**
     * When Special Member Delivery is selected,
     * make order note required field.
     */
    public function check_if_special_member_delivery( $fields ) {
        $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
        if ($chosen_methods[0] == 'local_pickup:5') {
            $fields['order']['order_comments']['required'] = true;
            $fields['order']['order_comments']['placeholder'] = 'Please enter the name of your church and district. Your order will be sent there.';
        }
        return $fields;
    }

    /**
     * Prevent changing shipping methods.
     *
     * Special Member Delivery or (Flat rate or local pickup, or free shipping)
     */
    public function shipping_toggle(){

        ?>
        <script type="text/javascript">
            jQuery( document ).ajaxComplete(function( event, xhr, settings ) {
                let selectedShipping = jQuery( 'select.shipping_method, input[name^="shipping_method"][type="radio"]:checked, input[name^="shipping_method"][type="hidden"]' )[0].value
                console.log(selectedShipping)
                if( selectedShipping === 'local_pickup:5') {
                    jQuery('#shipping_method_0_flat_rate1').parent().remove()
                    jQuery('#shipping_method_0_local_pickup3').parent().remove()
                    if ( jQuery('#shipping_method_0_free_shipping4').length ) {
                        jQuery('#shipping_method_0_free_shipping4').parent().remove()
                    }
                }else{
                    jQuery('#shipping_method_0_local_pickup5').parent().remove()
                }
            });

        </script>
        <?php
    }
}


