<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Invoice_Reference
{
    function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_filter('woocommerce_xero_invoice_to_xml', array($this, 'adjust_invoice_reference'), 10, 1);
    }

    public function adjust_invoice_reference($invoice_xml)
    {
        $xml = new SimpleXMLElement($invoice_xml);
        $order_id = array_pop(explode('-', $xml->InvoiceNumber));
        if (!($order = wc_get_order($order_id))) {
            // Do nothing if order don't exist.
            return $invoice_xml;
        }

        $xml->Reference .= $this->get_order_created_via($order) . $this->get_customers_locality($order);
        $xml_string = $xml->asXML();
        return substr($xml_string, strpos($xml_string, '?' . '>') + 2);
    }

    private function get_customers_locality(WC_Order $order)
    {
        $customer_id = $order->get_customer_id();
        if (get_userdata($customer_id)) {
            $locality = get_user_meta($customer_id, 'user_registration_locality', true);
            if (!empty($locality)) {
                return ",$locality";
            }
        }
        return '';
    }

    private function get_order_created_via(WC_Order $order)
    {
        $created_via = $order->get_created_via();
        $ref = '';
        switch ($created_via) {
            case 'Square':
                $ref = ',square-pos';
                break;
            case 'admin':
                $ref = ',stream-direct';
                break;
            case 'checkout':
                $ref = ',online';
                break;
            case 'rest-api':
                // get issue reference
                $ref = ',' . strtolower($order->get_customer_note());
                break;
        }

        return str_replace(' ', '-', $ref);
    }
}
