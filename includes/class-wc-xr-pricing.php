<?php

require_once ABSPATH . 'wp-content/plugins/customer-specific-pricing-for-woocommerce/includes/role-specific-pricing/class-wdm-wusp-simple-products-rsp.php';

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Pricing
{

    const DEFAULT_PRICE = "retail";
    const DECIMALS = 2;

    function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_filter('woocommerce_product_get_price', array($this, 'get_price'), 10, 2);
        add_filter('woocommerce_get_price_html', array($this, 'change_price_html'), 10, 2);
        add_action('woocommerce_process_product_meta_simple', array($this, 'updateMemberPrice'), 220, 1);
        add_filter('wc_square_sync_to_square_price', array($this, 'get_member_price'), 10, 2);

        if (isset($_POST['action']) && 'woocommerce_add_order_item' == $_POST['action']) {
            add_action('woocommerce_new_order_item', array($this,'changeTotalSubtotal'), 111, 3);
        }
    }

    /**
     * called when new order is created from backend
     */
    public function changeTotalSubtotal($item_id, $orderItem, $orderId)
    {
        // woocommerce backend order, when product is added.
        if($orderItem instanceof \WC_Order_Item_Product &&
            !empty($userId = get_post_meta($orderId, 'csp_customer_id', true)))
        {
            // Fetch the variation ID.
            $productId = $orderItem->get_variation_id();

            // If variation ID is '0', then fetch the product ID.
            if (empty($productId)) {
                $productId = $orderItem->get_product_id();
            }

            $product = wc_get_product( $productId );
            $productQuantity = $orderItem->get_quantity();
            $cspPrice = \WuspSimpleProduct\WuspCSPProductPrice::getDBPrice($productId, $product->get_price(), $productQuantity, $userId);

            // set product subtotal
            $orderItem->set_subtotal($productQuantity * wc_get_price_excluding_tax( $product, array('price' => $cspPrice) ));
            // set product total
            $orderItem->set_total($productQuantity *  wc_get_price_excluding_tax( $product, array('price' => $cspPrice) ) );
        }
    }

    public function get_member_price($price, WC_Product $product)
    {
        $price = $this->get_price_by_role($product,'member',0);
        return $price;
    }

    /**
     *
     * Use regular price as member price. Regular price overwrites the members price.
     * @param $product_id
     */
    public function updateMemberPrice($product_id)
    {
        $product = wc_get_product( $product_id );
        global $wpdb;
        $wpdb->query( "UPDATE wp_wusp_role_pricing_mapping SET price= '".$product->get_regular_price()."'  where product_id='".$product_id."' AND role = 'member'" );
    }

    /**
     *
     * For this to work there need to be at least one user per role, retail and member.
     *
     * @param $price_html
     * @param WC_Product $product
     * @return string
     */
    public function change_price_html($price_html, WC_Product $product)
    {

        $default_price = 0;
        $retail_price = $this->get_price_by_role($product, 'retail', $default_price);
        $member_price = $this->get_price_by_role($product, 'member', $default_price);
        $wholesale_price = $this->get_price_by_role($product, 'wholesale', $default_price);
        $price_html='';

        // if the current user is wholesale
        if ($this->current_user_has_role('wholesale') && $wholesale_price != 0 ) {
            $price_html = '<span class="amount"> $' . $wholesale_price . ' Wholesale price</span><br>';

        } else if( $retail_price != 0 ){
            $price_html = '<span class="amount"> $' . $retail_price . ' Guest price</span><br>';
        }

        $price_html .= '<span class="amount"> $' . $member_price . ' Member price</span>';

        return $price_html;
    }

    public function current_user_has_role($role)
    {
        if (is_user_logged_in()) {
            $user = wp_get_current_user();
            return in_array($role, (array)$user->roles);
        } else {
            return false;
        }
    }

    /**
     *
     * Get the retail price as the default price.
     *
     * If the user is not logged in change
     * the price to retail price. If the retail price does not exist,
     * price remains the same.
     *
     * The price in the database should be the price for the saints.
     *
     */
    public function get_price($price, $product)
    {
        if (!is_user_logged_in()) {
            return $this->get_price_by_role($product, self::DEFAULT_PRICE, $price);
        }
        return $price;
    }

    /**
     * @param WC_Product $product
     * @param $role
     * @param $default_price
     * @return mixed
     */
    public function get_price_by_role(WC_Product $product, $role, $default_price)
    {
        /** @WP_User  $user */
        $user = get_user_by('login', $role . '_user');
        if ($user !== false) {
            $_price = \WuspSimpleProduct\WrspSimpleProduct\WdmWuspSimpleProductsRsp::getPriceOfProductForRole($user->ID, $product->get_id());
            if ($_price !== false || $_price != 0) {
                return round($_price, self::DECIMALS);
            }
        } else {
            return $default_price;
        }
    }
}
