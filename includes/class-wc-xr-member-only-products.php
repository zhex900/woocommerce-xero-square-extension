<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 *
 * Explain to the customers that this product is only available to members
 *
 * Class WC_XR_Member_Only_Products
 *
 */
class WC_XR_Member_Only_Products extends WC_XR_Pricing
{

    function __construct()
    {
        parent::__construct();
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_action( 'woocommerce_single_product_summary', array($this,'message'), 20 );
    }

    public function message() {

        global $product;

        $retail_price = $this->get_price_by_role($product,'retail',-1);

        if ($retail_price == '' && !is_user_logged_in()  ){ // no retail price and not login
            echo '
            <style>
                .member-only {
                    /*color: #01579B*/
                    animation:blinkingText 1s infinite;
                }
                
                @keyframes blinkingText{
                    0%{     color: red;    }
                    50%{    color: pink; }
                    100%{   color: tomato;    }
                }
                </style>
            ';
            echo '<span class="member-only">This product is only available to registered customers. Please register or login.</span><br>';
        }
    }

}
