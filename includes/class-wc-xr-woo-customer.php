<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

abstract class WC_XR_Woo_Customer
{

    /**
     * Xero settings.
     *
     * @var WC_XR_Settings
     */
    protected $settings;
    /**
     * @var WC_XR_Logger
     */
    protected $logger;

    protected $new_customers;
    protected $existing_customers;

    const DEFAULT_ROLE ='member';

    function __construct(WC_XR_Settings $settings)
    {
        $this->settings = $settings;
        $this->logger = new WC_XR_Logger($this->settings);
        $this->new_customers=0;
        $this->existing_customers=0;
    }

    /**
     *
     * Create new woo customer.
     */
    public static function create_woo_customer($customer_data)
    {
        // Sets the username.
        $customer_data['username'] = !empty($customer_data['username']) ? $customer_data['username'] : self::get_username($customer_data['email']);
        // Sets the password.
        $customer_data['password'] = !empty($customer_data['password']) ? $customer_data['password'] : '';
        // Attempts to create the new customer.
        file_put_contents('/tmp/logs.log', 'create new user: '.$customer_data['email']. ' ('. $customer_data['username'].') '. $customer_data['password']  . PHP_EOL, FILE_APPEND);

        $customer_id = wp_create_user($customer_data['username'],$customer_data['password'], $customer_data['email']);

        // Checks for an error in the customer creation.
        if (is_wp_error($customer_id)) {
            // if fails use default POS
            return get_user_by('login', 'square_user');
        }

        // Added customer data.
        $customer = get_user_by('id',$customer_id);
        $customer->set_role(self::DEFAULT_ROLE);
        unset( $customer_data['password'] );
        self::update_woo_customer($customer_id, $customer_data);

        return $customer;
    }

    public static function sendNewUserEmail($customer_id,$customer_data){
        new WC_Emails();
        update_option( 'woocommerce_registration_generate_password', 'yes' );
        do_action( 'woocommerce_created_customer_notification', $customer_id, ['user_login'=>$customer_data['username'],'user_pass'=>$customer_data['password']], true );
        update_option( 'woocommerce_registration_generate_password', 'no' );
    }

    /**
     * Add/Update customer data.
     *
     * @param int $customer_id The customer ID
     * @param array $fields
     */
    public static function update_woo_customer($customer_id, $fields)
    {
        foreach ($fields as $key => $value){
            if( isset($value) ) {
                update_user_meta($customer_id, $key, wc_clean($value));
            }
        }

        wp_update_user( array( 'ID' => $customer_id, 'user_email' => $fields['email'] ) );
    }

    protected static function get_username($email){
        // use email prefix as username
        preg_match('/^(.*)@.*$/', strtolower($email), $output_array);
        $username=$output_array[1];

        /**
         * Generate an unique username.
         * If the email username already exists, append a random number to it.
         * This will continue until an unique username is found.
         */
        $tries = 0;
        $num_tries=10;
        while ( ( $exists = username_exists( $username ) ) && $tries++ < $num_tries ) {
            $username = $username . '_' . mt_rand();
        }

        return $username;
    }
}
