<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Correct_Order_Rounding_Error
{

    private const TAX_RATE = 11;
    private const TAX_RATE_PERCENT = 0.1;

    public function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_action('woocommerce_order_after_calculate_totals', array($this, 'correct_rounding_errors'), 1, 2);
    }

    private function parse_coupons(WC_Order $order)
    {
        $discounts = [];
        foreach ($order->get_coupons() as $item_coupon) {
            $coupon = new WC_Coupon($item_coupon->get_code());
            $couponType = $coupon->get_discount_type();
            $discounts[$couponType] = $coupon->get_amount() +
                (array_key_exists($couponType, $discounts) ? $discounts[$coupon->get_discount_type()] : 0);
        }
        return $discounts;
    }

    private function get_total_discount_total_exclude_tax($item_total_exclude_tax, $discounts, $index)
    {
        $total_discount = 0;
        foreach ($discounts as $type => $amount) {
            if ($type === 'percent') {
                $total_discount += round($item_total_exclude_tax * ($amount / 100), 4);
            }
            if ($type === 'fixed_product') {
                $total_discount += round($amount / 1.1, 4);
            }
            // apply cart discount to the first item
            if ($type === 'fixed_cart' && $index === 1) {
                $total_discount += round($amount / 1.1, 4);
            }
        }
        return $total_discount;
    }

    public function correct_rounding_errors($and_taxes, $order): void
    {
        if (!$order instanceof WC_Order) {
            return;
        }

        $discounts = $this->parse_coupons($order);
        $line_total = $this->get_order_fees_total($order) + (float)$order->get_shipping_total() + (float)$order->get_shipping_tax();
        $total_discount_total_exclude_tax = 0;
        $total_discount_tax = 0;
        $i = 1;
        foreach ($order->get_items() as $item) {
            /** @var WC_Order_Item_Product $item */
            $price = $item->get_product()->get_regular_price();

            $item_total = $price * $item->get_quantity();
            $line_total += $item_total;
            $item_tax = round($item_total / self::TAX_RATE, 4);
            $item_exclude_tax = $item_total - $item_tax;

            $discount_total_exclude_tax = $this->get_total_discount_total_exclude_tax($item_exclude_tax, $discounts,
                $i);

            $discount_tax = $discount_total_exclude_tax * self::TAX_RATE_PERCENT;
            $total_discount_total_exclude_tax += $discount_total_exclude_tax;
            $total_discount_tax += $discount_tax;
            $item_total_exclude_tax = round($item_exclude_tax - $discount_total_exclude_tax, 4);
            $item->set_subtotal($item_total_exclude_tax);
            $item->set_total($item_total_exclude_tax);

            $item_unit_amount = $order->get_item_subtotal($item, false, true) * $item->get_quantity();
            $diff = $item_unit_amount - $item_total_exclude_tax;
            $tax = $item_tax - $diff - $discount_tax;


            $item->set_total_tax($tax);
            $item->set_subtotal_tax($tax);
            $item->set_taxes([
                'total' => [1 => $tax],
                'subtotal' => [1 => $tax]
            ]);
            $item->save(); // Save line item data
            $i++;
        }
        $order->set_discount_tax(round($total_discount_tax, 2));
        $order->set_discount_total(round($total_discount_total_exclude_tax + $total_discount_tax, 2));
        $order->set_total(round($line_total - $total_discount_total_exclude_tax - $total_discount_tax, 2));
        $order->save();
    }

    protected function get_order_fees_total(WC_Order $order)
    {
        $fees_total = 0;
        foreach ($order->get_fees() as $item) {
            $fees_total += round((float)$item->get_total() + (float)$item->get_total_tax(), 2);
        }

        return round($fees_total, 2);
    }
}
