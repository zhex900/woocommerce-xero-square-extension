<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class WC_XR_Default_Product_Weight_Dimension
{
    const DEFAULT_WEIGHT = 1;
    const DEFAULT_HEIGHT = 10;
    const DEFAULT_LENGTH = 10;
    const DEFAULT_WIDTH = 10;

    public function __construct()
    {
        add_filter('woocommerce_product_get_length', [$this, 'product_default_length']);
        add_filter('woocommerce_product_get_width', [$this,'product_default_width']);
        add_filter('woocommerce_product_get_height', [$this,'product_default_height']);
        add_filter('woocommerce_product_get_weight', [$this,'product_default_weight']);
    }

    public function product_default_length($length)
    {
        if (empty($length)) {
            return self::DEFAULT_LENGTH;
        } else {
            return $length;
        }
    }

    public function product_default_width($width)
    {
        if (empty($width)) {
            return self::DEFAULT_WIDTH;
        } else {
            return $width;
        }
    }

    public function product_default_height($height)
    {
        if (empty($height)) {
            return self::DEFAULT_HEIGHT;
        } else {
            return $height;
        }
    }

    public function product_default_weight($weight)
    {
        if (empty($weight)) {
            return self::DEFAULT_WEIGHT;
        } else {
            return $weight;
        }
    }
}
