<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WC_XR_User_Registration {
	private $prefix = 'user_registration_';
	private $address_fields = [
		'_address_1',
		'_city',
		'_postcode',
		'_state',
	];

	function __construct() {
		$this->setup_hooks();
	}

	public function setup_hooks() {
		add_action( 'user_register', [ $this, 'add_locality_field' ] );
		add_action( 'added_user_meta', [ $this, 'save_address' ], 20, 4 );
		add_filter(
			'user_registration_profile_meta_fields', [ $this, 'user_profile_fields' ], 10 );
	}

	/**
	 * Add user registration form id to the user
	 * When a new user is created from admin page the user
	 * is not linked with the registration form. So
	 * the locality field is not rendered.
	 *
	 * @param $user_id
	 */
	public function add_locality_field( $user_id ) {
		$registration_form = get_user_meta( $user_id, 'ur_form_id', true );
		if ( $registration_form === '' ) {
			global $wpdb;
			// get the registration form id
			$registration_form_id = $wpdb->get_results( "SELECT ID from wp_posts where post_title='Registration' and post_status='publish' and post_type='user_registration'" );
			if ( count( $registration_form_id ) > 0 ) {
				add_user_meta( $user_id, 'ur_form_id', $registration_form_id[0]->ID );
			}
		}
	}

	/**
	 * Let's change the nickname
	 *
	 * @param int $mid Meta ID
	 * @param $user_id
	 * @param string $meta_key Meta key.
	 * @param mixed $meta_value Meta value.
	 */
	public function save_address( $mid, $user_id, $meta_key, $meta_value ) {

		$saved = false;
		foreach ( $this->address_fields as $field ) {
			if ( $this->prefix . 'billing' . $field === $meta_key ) {
				$saved = true;
				if ( $field === '_state' ) {
					update_user_meta( $user_id, 'shipping' . $field, strtoupper( $meta_value ) );
					update_user_meta( $user_id, 'billing' . $field, strtolower( $meta_value ) );
				} else {
					update_user_meta( $user_id, 'shipping' . $field, ucwords( strtolower( $meta_value ) ) );
					update_user_meta( $user_id, 'billing' . $field, ucwords( strtolower( $meta_value ) ) );
				}
			}
		}
		if ( $saved ) {
			$first_name = ucwords( strtolower( get_user_meta( $user_id, 'first_name', true ) ) );
			$last_name  = ucwords( strtolower( get_user_meta( $user_id, 'last_name', true ) ) );
			update_user_meta( $user_id, 'shipping_first_name', $first_name );
			update_user_meta( $user_id, 'shipping_last_name', $last_name );
			update_user_meta( $user_id, 'billing_first_name', $first_name );
			update_user_meta( $user_id, 'billing_last_name', $last_name );
			update_user_meta( $user_id, 'first_name', $first_name );
			update_user_meta( $user_id, 'last_name', $last_name );
		}
	}

	public function user_profile_fields( $fields ) {

		$locality = $fields['user_registration']['fields']['user_registration_locality'];

		return [
			'user_registration' => [
				'title'  => 'User\'s church or district',
				'fields' => [ 'user_registration_locality' => $locality, ],
			]
		];
	}
}
