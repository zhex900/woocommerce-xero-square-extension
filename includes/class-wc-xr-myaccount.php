<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_My_Account
{

    function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_filter('woocommerce_my_account_my_orders_actions', array($this, 'my_account_invoice_link'), 10, 2);
        add_filter('woocommerce_new_customer_data', array($this, 'change_default_customer_role'), 10, 1);
        add_action('user_register', [$this, 'change_default_user_role']);

        // prevent user auto login after successful registration
        add_filter('woocommerce_registration_redirect', array($this, 'user_autologout'), 12);
        add_action('woocommerce_before_customer_login_form', array($this, 'successful_registration_message'), 9);
    }

    function user_autologout($redirect_url)
    {

        if (is_user_logged_in()) {
            wp_logout();
            return get_permalink(woocommerce_get_page_id('myaccount')) . "?registered=true";
        }
        return $redirect_url;

    }


    function successful_registration_message()
    {

        if (isset($_REQUEST['registered']) && $_REQUEST['registered'] == 'true') {
            unset($_REQUEST['registered']);
            wc_clear_notices();
            wc_add_notice(__('You have successfully registered! Your username and password is sent to your email address.', 'woocommerce'), 'success');
        }

    }

    /**
     * Display invoice download link on My Account page
     */
    public function my_account_invoice_link($actions, $order)
    {

        $order_id = $order->get_id();
        $invoice_url = wp_nonce_url(admin_url("admin-ajax.php?action=get_invoice&order_id=$order_id"), 'get_invoice');
        $button_text = __('Download invoice (PDF)', 'woocommerce-xero-square-extension');
        $actions['invoice'] = array(
            'url' => $invoice_url,
            'name' => apply_filters('WC_Xero_Square_Extension_myaccount_button_text', $button_text, null)
        );

        return apply_filters('WC_Xero_Square_Extension_myaccount_actions', $actions, $order);
    }

    public function change_default_user_role($user_id)
    {
        $user = get_user_by('id', $user_id);

        $user->set_role(get_option('default_role'));
    }

    public function change_default_customer_role($customer)
    {

        $customer['role'] = get_option('default_role');

        return $customer;
    }
}