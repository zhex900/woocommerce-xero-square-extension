<?php
/**
 * @package WC_Xero_Square_Extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

use Xero_Extension\WC_XR_Get_Invoice;

/**
 * Attach invoice to emails
 */
class WC_XR_Attach_Invoice_To_Email
{

    public function __construct()
    {
        $this->setup_hooks();
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('woocommerce_before_resend_order_emails', [$this, 'add_attach_invoice']);
        add_action('woocommerce_after_resend_order_email', [$this, 'remove_attach_invoice']);
        add_action('xero_extension_before_invoice_to_customer', [$this, 'add_attach_invoice']);
        add_action('xero_extension_after_invoice_to_customer', [$this, 'remove_attach_invoice']);
    }

    public function add_attach_invoice()
    {
        add_filter('woocommerce_email_attachments', array($this, 'attach_invoice_to_email'), 199, 3);
    }

    public function remove_attach_invoice()
    {
        remove_filter('woocommerce_email_attachments', array($this, 'attach_invoice_to_email'), 199, 3);
    }

    /**
     * Attach PDF to WooCommerce email
     */
    public function attach_invoice_to_email($attachments, $email_id, $order)
    {
        // check if all variables properly set
        if (!is_object($order) || !isset($email_id)) {
            return $attachments;
        }

        // Skip User emails
        if (get_class($order) == 'WP_User') {
            return $attachments;
        }

        $order_id = $order->get_id();

        if (get_class($order) !== 'WC_Order' && $order_id == false) {
            return $attachments;
        }

        // do not process low stock notifications, user emails etc!
        if (in_array($email_id, array('no_stock', 'low_stock', 'backorder', 'customer_new_account', 'customer_reset_password')) || get_post_type($order_id) != 'shop_order') {
            return $attachments;
        }

        do_action('WC_Xero_Square_Extension_before_attachment_creation', $order, $email_id, 'pdf');

        try {

//            file_put_contents('/tmp/xero.log', "attach_invoice_to_email order $order_id, " .$order->get_status() . PHP_EOL, FILE_APPEND);
            list (, , $invoice_path) = WC_XR_Get_Invoice::get_xero_invoice($order_id, true);
            $attachments[] = $invoice_path;
            do_action('WC_Xero_Square_Extension_email_attachment', $invoice_path, 'pdf', '');
        } catch (Exception $e) {
            error_log($e->getMessage());
        }

        return $attachments;
    }
}
