<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Sync_Product_Square
{

    const TEMP_ATTACHMENT_PATH = '/tmp/attachment_temp';

    function __construct()
    {
        $this->setup_hooks();
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_filter( 'get_attached_file', [$this,'get_local_copy_of_attached_file'], 101, 2 );
    }

    /**
     *
     * This is a workaround for retrieving attachments stored on S3.
     * Write the attachment stored in S3 locally. Return the local path.
     *
     * @param string $file S3 attachment file path
     * @param $attachment_id
     * @return string local path of the attachment
     *
     */
    public function get_local_copy_of_attached_file( $file, $attachment_id ) {
        if( $file != "" ) {
            file_put_contents(self::TEMP_ATTACHMENT_PATH, file_get_contents($file));
        }
        return self::TEMP_ATTACHMENT_PATH;
    }
}
