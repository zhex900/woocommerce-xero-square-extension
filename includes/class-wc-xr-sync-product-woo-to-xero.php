<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

use Xero_Extension\WC_XR_Get_Invoice;

class WC_XR_Sync_Product_Woo_To_Xero
{
    const MAX_LIMIT = 500; // Xero API request limit
    const XERO_SYNC = '_xero_product_sync_copy';
    private $SUCCESS_OR_FAILED = [true => 'Success', false => 'Failed'];
    /**
     * Xero settings.
     *
     * @var WC_XR_Settings
     */
    private $settings;
    /**
     * @var WC_XR_Logger
     */
    private $logger;

    function __construct(WC_XR_Settings $settings)
    {
        $this->settings = $settings;
        $this->logger = new WC_XR_Logger($this->settings);
        $this->setup_hooks();
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('wp_ajax_sync_woo_to_xero_product', [$this, 'sync_woo_to_xero_product_ajax']);
        add_action('woocommerce_new_product', [$this, 'sync_product']);
        add_action('woocommerce_update_product', [$this, 'sync_product']);
    }

    /**
     *
     * Sync all WooCommerce products with Xero
     *
     * Loop through all WooCommerce products page by page. Each page will have
     * MAX_LIMIT of products. Loop will stop when no products are in the page.
     *
     * Product sync is applied at each iteration.
     *
     */
    public function sync_woo_to_xero_product_ajax()
    {
        $result = [];
        $i = 1;
        while (true) {
            $products = $this->get_woo_products_per_page($i, self::MAX_LIMIT);
            $size = count($products);

            if ($products && $size > 0) {
                $result[] = ['result' => $this->sync_products($products, true), 'size' => $size];
                $i++; // go to the next page to retrieve next batch of products
            } else {
                break;
            }

            // there is no need to go to the next page, since
            // the product limit is not reached.
            if ($size < self::MAX_LIMIT) {
                break;
            }
        }

        $this->pretty_print($result);
        die;
    }

    private function pretty_print($result)
    {
        $i = 1;
        $total_products = 0;
        foreach ($result as $r) {
            echo "Page $i, number of products " . $r['size'] . " -> " . $this->SUCCESS_OR_FAILED[$r['result']] . PHP_EOL;
            $i++;
            $total_products += $r['size'];
        }
        echo "Total products: $total_products" . PHP_EOL;
    }

    /**
     *
     * Sync a WC_Product with Xero.
     *
     * @param $product_id
     * @return bool
     *
     */
    public function sync_product($product_id)
    {
        // only sync when there is no pending xero actions
        if (WC_XR_Get_Invoice::xero_pending_action_exists()) {
            return false;
        }

        $product = (new WC_Product_Factory())->get_product($product_id);

        if (!is_null($product)) {
            return $this->sync_products([$product]);
        }
        return false;
    }

    /**
     * Check whether the product require to be synced with xero
     *
     * @param WC_Product $product
     * @return bool
     */
    private function isInSyncWithXero(WC_Product $product)
    {
        $productId = $product->get_id();
        if (!metadata_exists('post', $productId, self::XERO_SYNC)) {
            return false;
        }
        $xeroCopy = get_post_meta($product->get_id(), self::XERO_SYNC, true);
        return $xeroCopy === $this->getProductXeroCopy($product);
    }

    /**
     * @param WC_Product $product
     * @return string
     */
    private function getProductXeroCopy(WC_Product $product)
    {
        return $product->get_name() . $product->get_regular_price() . $product->get_sku();
    }

    /**
     * save a copy of product information that is saved in xero
     * @param array $products
     */
    public function saveXeroSyncCopy(array $products)
    {
        foreach ($products as $product) {
            update_post_meta($product->get_id(), self::XERO_SYNC, $this->getProductXeroCopy($product));
        }
    }

    /**
     *
     * Sync an array of WC_Product with Xero.
     * Existing Xero product will be updated.
     * Non-existing Xero product will be created.
     *
     * Only these attributes are synced.
     *
     * SKU
     * Product name
     * Price will be zero
     * Tax code is OUTPUT
     * Account code is 200 Sales
     * Description: Woo product id
     *
     * @param array WC_Product $products
     * @param bool $force
     * @return bool
     */
    public function sync_products(array $products, $force = false)
    {
        // filter out products don't need to be synced.
        if (!$force) {
            $products = array_filter($products, function ($product) {
                return !$this->isInSyncWithXero($product);
            });
        }
        if (count($products) === 0) {
            return true;
        }
        $this->saveXeroSyncCopy($products);
        $inventory = new WC_XR_Product($products);
        $inventory->set_sales_account_code($this->settings->get_option('sales_account'));
        $request = new WC_XR_Request_Sync_Product($this->settings, $inventory);
        return WC_XR_Request_Actions::send_(
            $request,
            $this->logger,
            function ($xml_response) {
                // save the xero inventory id into product meta data
                foreach ($xml_response->Items->Item as $item) {
                    // product id is saved into description
                    $product_id = ((array)$item->Description)[0];
                    update_post_meta($product_id, '_xero_inventory_id', esc_attr($item->ItemID));
                }
                return true;
            });
    }

    /**
     *
     * Get an array of published products page by page.
     *
     * @param int $page . Page number.
     * @param int $limit . The number of product per page
     * @return array of WC_Product
     *
     */
    protected function get_woo_products_per_page($page, $limit)
    {
        $args = [
            'status' => ['publish'],
            'limit' => $limit,
            'page' => $page,
            'return' => 'objects'
        ];
        return wc_get_products($args);
    }
}
