<?php

require_once ABSPATH . 'wp-content/plugins/woocommerce/packages/action-scheduler/classes/abstracts/ActionScheduler.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce/packages/action-scheduler/classes/abstracts/ActionScheduler_Store.php';

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


/**
 * *
 */
class WC_XR_Scheduler
{
    public $MAX_RETRY = 3;
    public $RATE_LIMIT_EXCEEDED = 'rate limit exceeded';
    public $DEFAULT_DATETIME_FORMAT = 'd/m/Y h:i:s A';
    public $USER_TIMEZONE = "Australia/Sydney";
    public $MAX_WAIT_TIME = 15; // seconds
    private $MIN_WAIT_TIME = 15; // seconds
    private $LAST_ACTION_LOG = '/tmp/last_action.log';

    public function fire($action, $order_id)
    {
        $result = false;
        $retry = 1;
        $buffer = 0;
        while ($retry == 1 || ($this->is_rate_limit_exceeded($order_id) && $retry < $this->MAX_RETRY)) {
            if ($retry > 1) {
                $buffer = 61;
            }
            $this->wait($this->MIN_WAIT_TIME * $retry + $buffer);
            $time = date('d/m/Y H:i:s', time());
            // file_put_contents('/tmp/xero.log', "fire ". $action[1]." $order_id $time ($retry)" . PHP_EOL, FILE_APPEND);
            $result = call_user_func($action, $order_id);
            ++$retry;
        }
        return $result;
    }

    public function wait($min_wait_time)
    {
        if (file_exists($this->LAST_ACTION_LOG)) {
            $last_action_timestamp = (int)file_get_contents($this->LAST_ACTION_LOG);
            $now = time();
            $diff = $min_wait_time - abs($now - $last_action_timestamp);
            $d = $now - $last_action_timestamp;

            if ($diff > 0) {
                //     file_put_contents('/tmp/xero.log',
                //       "---- wait ----- $diff seconds $now $last_action_timestamp $d" . PHP_EOL, FILE_APPEND);
                sleep($diff);
            } else {
                // file_put_contents('/tmp/xero.log',
                //   "---- skip wait ----- $diff seconds $now $last_action_timestamp $d" . PHP_EOL, FILE_APPEND);

            }
        }
        file_put_contents($this->LAST_ACTION_LOG, time());
    }

    /**
     * The scheduler is implemented via the cron job. The cron job runs every minute. Two
     * jobs per minute. This function doesn't really do anything.
     * @return int
     */
    public function next_scheduled_time()
    {
//        $wait_time = rand($this->MAX_WAIT_TIME, $this->MIN_WAIT_TIME);
//
//        $scheduled_actions = as_get_scheduled_actions([
//            'group' => 'xero',
//            'orderby' => 'date',
//            'date_compare' => '<',
//            'order' => 'ASC',
//            'per_page' => 9999999,
//            'status' => ActionScheduler_Store::STATUS_PENDING
//        ]);

        $next_schedule = time();

//        if (count($scheduled_actions) > 0) {
//            $last_action = end(array_values($scheduled_actions));
//            $timestamp = array_values((array)(array_values((array)$last_action)[2]))[1];
//            $next_schedule = (int)$timestamp + $wait_time;
//        }
        return $next_schedule;
    }

    public function is_rate_limit_exceeded($order_id)
    {
        $latest_notes = current(wc_get_order_notes(
            array(
                'order_id' => $order_id,
                'limit' => 1,
                'orderby' => 'date_created_gmt',
            )
        ));

        //if true reschedule everything. push everything back by 10 minutes.
        return preg_match("/{$this->RATE_LIMIT_EXCEEDED}/i", $latest_notes->content);
    }

    public function timestamp_to_user_date($timestamp)
    {
        $dt = new DateTime();
        $dt->setTimestamp($timestamp);
        $dt->setTimezone(new DateTimeZone($this->USER_TIMEZONE));
        return $dt->format($this->DEFAULT_DATETIME_FORMAT);
    }

    public function schedule_order_action($action, $order_id, $note, $retry)
    {
        $scheduled_time = $this->next_scheduled_time(); //UTC timestamp
        $user_scheduled_time = $this->timestamp_to_user_date($scheduled_time);
        // add schedule note to the order
        $order = wc_get_order($order_id);
        $order->add_order_note(__($note,
                'wc-xero') . "Scheduled time $user_scheduled_time.");
        as_schedule_single_action($scheduled_time, $action, array($order_id, $retry), 'xero');
    }
}
