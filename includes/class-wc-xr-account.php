<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class WC_XR_Account
{
    private $account;

    public function __construct(array $account)
    {
        $this->account = $account;
    }

    /**
     * Return XML of Manual Journal
     * Debits are positive, credits are negative value
     * @return string
     */
    public function to_xml()
    {
        $xml = '<Account>';
        foreach ($this->account as $key => $value) {
            $xml .= "<$key>$value</$key>";
        }
        $xml .= '</Account>';
        return $xml;
    }
}
