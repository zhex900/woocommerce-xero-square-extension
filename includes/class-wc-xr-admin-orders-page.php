<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

CONST DEFAULT_PAYMENT_METHOD = 'cod';
use Xero_Extension\WC_XR_Get_Invoice;

class WC_XR_Admin_Orders_Page
{
    function __construct()
    {

        add_filter('manage_edit-shop_order_columns', array($this, 'add_invoice_number_column'), 999);
        add_filter('manage_edit-shop_order_columns', array($this, 'add_invoice_number_status'), 999);
        add_action('manage_shop_order_posts_custom_column', array($this, 'invoice_number_column_data'), 2);

        add_filter('woocommerce_shop_order_search_fields', array($this, 'search_fields'));

        add_action('woocommerce_process_shop_order_meta', array($this, 'send_emails'), 60, 2);
        add_action('woocommerce_admin_order_data_after_shipping_address', array($this, 'add_xero_invoice_number_to_order_details'), 10, 1);

        add_filter('manage_edit-shop_order_sortable_columns', array($this, 'invoice_number_column_sortable'));
        add_filter('request', array($this, 'request_query_sort_by_invoice_number'));
        add_action('admin_head', array($this, 'remove_wanted_fields'));
        add_action('admin_head', array($this, 'set_default_payment_method'));

        add_filter( 'wc_order_statuses', array($this, 'rename_order_status' ));

        add_action('admin_head', array($this, 'enforce_save_order_before_add_products'));
    }

    public function rename_order_status( $order_statuses ) {
        foreach ( $order_statuses as $key => $status ) {
            if ( 'wc-completed' === $key )
                $order_statuses['wc-completed'] = _x( 'Dispatched', 'Order status', 'woocommerce' );
        }
        return $order_statuses;
    }

    public function set_default_payment_method()
    {
        echo '
            <script type="text/javascript">
                 jQuery(document).ready(function(){
                     if( window.location.href.includes("post-new.php?post_type=shop_order") && jQuery(\'#order_status\').val() === "wc-pending" && jQuery("#xero-invoice-number")[0].innerText == ""){
                        jQuery("#_payment_method > option").each(function() {
                            if ( jQuery(this).val() != "'.DEFAULT_PAYMENT_METHOD.'" ) {
                                jQuery(this).remove();
                            }
                        });
                     }
                 })
            </script>
        ';
    }

    /**
     * This is a work around solution. The problem is when products added to the order without
     * any customer there the price have a discount. I don't know why. However, if the order
     * is saved and have a customer this issue goes away.
     */
    public function enforce_save_order_before_add_products(){
        $order_id = $_GET['post'];

        if (empty($userId = get_post_meta($order_id, '_customer_user', true)) ) {
            echo '
              <script type="text/javascript">
                 jQuery(document).ready(function(){
                     
                     jQuery(".add-items").click(function (e) {
                         alert("Please select customer and save the order before adding products!");
                         e.stopPropagation();
                     })
                 })
            </script>';
        }

    }
    /**
     * remove
     * Custom fields
     * Downloadable product permissions
     */
    public function remove_wanted_fields()
    {
        echo '
              <script type="text/javascript">
                 jQuery(document).ready(function(){
                     jQuery("#postcustom").remove()
                     jQuery("#woocommerce-order-downloads").remove()
                 })
            </script>';
    }

    public function add_xero_invoice_number_to_order_details($order)
    {
        $invoice_number = WC_XR_Get_Invoice::get_xero_invoice_number_by_order_id($order->get_id(),$order->get_meta('_xero_invoice_id'));
        $invoice_status = $order->get_meta('_xero_invoice_status');
        // add invoice download link
        $url = wp_nonce_url(admin_url("admin-ajax.php?action=get_invoice&order_id=" . $order->get_id() . ""), 'get_invoice');

        ?>
        </div></div>
        <div class="clear"></div>
        <div class="order_data_column_container">
        <div class="order_data_column_wide">
        <h3><?php _e("Xero Invoice ({$invoice_status})#: <a id=\"xero-invoice-number\" href=".$url.">". $invoice_number); ?></a></h3>
        <p></p>
        <?php
    }

    /**
     * Create additional Shop Order column for Invoice Numbers
     * @param array $columns shop order columns
     */
    public function add_invoice_number_column($columns)
    {
        // put the column after the Status column
        $new_columns = array_slice($columns, 0, 2, true) +
            array('xero_invoice_number' => __('Invoice#', 'woocommerce-pdf-invoices-packing-slips')) +
            array_slice($columns, 2, count($columns) - 1, true);
        return $new_columns;
    }

    public function add_invoice_number_status($columns)
    {
        // put the column after the Status column
        $new_columns = array_slice($columns, 0, 2, true) +
            array('xero_invoice_status' => __('Invoice Status', 'woocommerce-pdf-invoices-packing-slips')) +
            array_slice($columns, 2, count($columns) - 1, true);
        return $new_columns;
    }

    /**
     * Display Invoice Number in Shop Order column (if available)
     * @param  string $column column slug
     */
    public function invoice_number_column_data($column)
    {
        global $the_order;

        if ($column == 'xero_invoice_number') {
            echo WC_XR_Get_Invoice::get_xero_invoice_number_by_order_id($the_order->get_id(),$the_order->get_meta('_xero_invoice_id'));
        }
        if ($column == 'xero_invoice_status') {
            echo $the_order->get_meta('_xero_invoice_status');
        }
    }

    /**
     * Add invoice number to order search scope
     */
    public function search_fields($custom_fields)
    {
        $custom_fields[] = '_xero_invoice_number';
        $custom_fields[] = '_xero_invoice_status';
        return $custom_fields;
    }

    /**
     * Add invoice number to order search scope
     */
    public function invoice_number_column_sortable($columns)
    {
        $columns['xero_invoice_number'] = 'xero_invoice_number';
        $columns['xero_invoice_status'] = 'xero_invoice_status';
        return $columns;
    }

    /**
     * WC3.X+ sorting
     */
    public function request_query_sort_by_invoice_number($query_vars)
    {
        global $typenow;

        if (in_array($typenow, wc_get_order_types('order-meta-boxes'), true)) {
            if (isset($query_vars['orderby'])) {
                if ('xero_invoice_number' === $query_vars['orderby']) {
                    $query_vars = array_merge($query_vars, array(
                        'meta_key' => '_xero_invoice_number',
                        'orderby' => 'meta_value',
                    ));
                }
                if ('xero_invoice_status' === $query_vars['orderby']) {
                    $query_vars = array_merge($query_vars, array(
                        'meta_key' => '_xero_invoice_status',
                        'orderby' => 'meta_value',
                    ));
                }
            }
        }

        return $query_vars;
    }
}
