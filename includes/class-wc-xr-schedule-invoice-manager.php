<?php
/**
 * Xero Credit Card Fee Manager class.
 *
 * @package WC_Xero_Square_Extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

require_once ABSPATH . 'wp-content/plugins/woocommerce-xero/includes/class-wc-xr-invoice-manager.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-schedule-payment-manager.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-scheduler.php';

/**
 * Xero Credit Card Fee Manager.
 */
class WC_XR_Schedule_Invoice_Manager extends WC_XR_Scheduler {

	/**
	 * Xero settings.
	 *
	 * @var WC_XR_Settings
	 */
	private $settings;

	private $invoice_manager;

	private $scheduler_payment_manager;

	/**
	 * WC_XR_Payment_Manager constructor.
	 *
	 * @param WC_XR_Settings $settings Xero settings.
	 */
	public function __construct( WC_XR_Settings $settings ) {
		$this->settings = $settings;
		$this->setup_hooks();
		$this->invoice_manager           = new WC_XR_Invoice_Manager( $this->settings );
		$this->scheduler_payment_manager = new WC_XR_Schedule_Payment_Manager();
	}

	/**
	 * Set up callbacks to the hooks.
	 */
	public function setup_hooks() {
		add_action( 'woocommerce_order_status_pending_to_processing', array( $this, 'schedule_send_invoice' ), 20, 1 );
		add_action( 'woocommerce_order_status_pending_to_completed', array( $this, 'schedule_send_invoice' ), 20, 1 );
		add_action( 'send_invoice_action', array( $this, 'send_invoice' ), 10, 2 );
	}

	private function removeDuplicateFees( $order_id ) {
		/** @var WC_Order $order */
		if ( ! ( $order = wc_get_order( $order_id ) ) ) {
			// Do nothing if order don't exist.
			return;
		}

		$duplicate_fees = [];
		$unique_fees    = [];
		foreach ( $order->get_items( 'fee' ) as $item_id => $item_fee ) {
			if ( ! in_array( $item_fee->get_name(), $unique_fees, true ) ) {
				$unique_fees[] = $item_fee->get_name();
			} else {
				$duplicate_fees[] = $item_id;
			}
		}
		foreach ( $duplicate_fees as $item_id ) {
			$order->remove_item( $item_id );
		}

		$order->calculate_totals();
		$order->save();
	}

	public function schedule_send_invoice( $order_id, $retry = 1 ) {
		$this->removeDuplicateFees( $order_id );

		$this->schedule_order_action( 'send_invoice_action', $order_id, 'Scheduled to create Xero Invoice.  ', $retry );
	}

	public function send_invoice( $order_id ) {
		if ( $this->fire( [ $this->invoice_manager, 'send_invoice' ], $order_id ) ) {
			// schedule payment after invoice is created.
			$this->scheduler_payment_manager->schedule_send_payment( $order_id );
		}
	}
}
