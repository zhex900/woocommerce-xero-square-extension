<?php

Class WC_XR_API_Xero_Oauth2
{
    public function __construct()
    {
        add_action('rest_api_init', function () {
            register_rest_route('xero-oauth2/', 'token', array(
                'methods' => 'GET',
                'callback' => array($this, 'get_xero_oauth2')
            ));
        });
    }

    /**
     *
     * @param WP_REST_Request $request
     *
     * @return array
     */
    public function get_xero_oauth2(WP_REST_Request $request)
    {
        $client_id = get_option('wc_xero_client_id', '');
        $client_secret = get_option('wc_xero_client_secret', '');
        $xero_oauth = WC_XR_OAuth20::get_instance($client_id, $client_secret);

        return [
            'token' => $xero_oauth->get_access_token(),
            'tenantId' => $xero_oauth->get_xero_tenant_id()
        ];
    }

}
