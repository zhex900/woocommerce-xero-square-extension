<?php
/**
 * Xero Credit Card Fee Manager class.
 *
 * @package WC_Xero_Square_Extension
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

use Xero_Extension\WC_XR_Get_Invoice;

require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/requests/class-wc-xr-request-actions.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/requests/class-wc-xr-request-manual-journal.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-manual-journal.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-gateway-stripe/includes/class-wc-stripe-helper.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero/includes/class-wc-xr-logger.php';
//const SQUARE_FEE = 1.9;

/**
 * Xero Credit Card Fee Manager.
 */
class WC_XR_Credit_Card_Fee_Manager
{

    /**
     * Xero settings.
     *
     * @var WC_XR_Settings
     */
    private $settings;
    /**
     * @var WC_XR_Logger
     */
    private $logger;

    /**
     * WC_XR_Payment_Manager constructor.
     *
     * @param WC_XR_Settings $settings Xero settings.
     */
    public function __construct(WC_XR_Settings $settings = null)
    {
        if (!is_null($settings)) {
            $this->settings = $settings;
            $this->setup_hooks();
            $this->logger = new WC_XR_Logger($this->settings);
        }
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        /**
         * Create a manual journal that moves the credit card fee amount out of the clearing account and into an expense account
         *
         * https://developer.xero.com/documentation/api-guides/handling-payment-processor-receipts-in-xero
         *
         * Credit clearing account
         * Debit fees expense account
         */

        // fire when xero manual payment action is clicked
        add_action('woocommerce_order_action_xero_manual_payment', array($this, 'send_credit_card_fee_manually'), 30, 1);

        // manual credit card action
        add_action( 'woocommerce_order_actions', array($this,'add_order_action') );
        add_action( 'woocommerce_order_action_send_credit_card_fee_manually', array($this,'send_credit_card_fee_manually'));

    }

    /**
     * Send the credit card fee as manual journal to the XERO API.
     *
     * @param int $order_id Order ID.
     *
     * @return bool
     */
    public function send_credit_card_fee($order_id)
    {
        if (!$order_id) {
            return;
        }

        // Getting an instance of the order object
        $order = wc_get_order($order_id);

        // If no fees exit
        if ($this->get_credit_card_fee($order) <= 0) {
            return;
        }

        do_action('wc_xero_send_credit_card_fee', $order_id);

        // Check for an invoice first.
        $invoice_id = $order->get_meta('_xero_invoice_id', true);
        $invoice_number = "INVW-$order_id";
        $payment_id = $order->get_meta('_xero_payment_id', true);

        if (!$invoice_id) {
            $order->add_order_note(__('Xero Credit Card Fee not created: Invoice has not been sent.', 'wc-xero'));
            return false;
        }
        if (!$payment_id) {
            $order->add_order_note(__('Xero Credit Card Fee not created: Payment has not been sent.', 'wc-xero'));
            return false;
        }
        if ($cc_id = $order->get_meta('_xero_manual_journal_credit_card_fee_id')){
            $order->add_order_note(__("Xero Credit Card Fee is already posted. $cc_id", 'wc-xero'));
            return false;
        }
        // Bank transaction Request.
        $manual_journal_request = new WC_XR_Request_ManualJournal($this->settings, $this->get_credit_card_fee_by_order($order,$invoice_number));

        return WC_XR_Request_Actions::send(
            $order,
            $manual_journal_request,
            $this->logger,
            function (WC_Order $order, $xml_response) {
                $manual_journal_id = (string)$xml_response->ManualJournals->ManualJournal->ManualJournalID;
                $order->update_meta_data('_xero_manual_journal_credit_card_fee_id', $manual_journal_id);
                $order->save_meta_data();
                $order->add_order_note(sprintf(
                /* translators: Payment ID from Xero. */
                    __('Xero credit card fee created. Transaction ID: %s', 'wc-xero'),
                    $manual_journal_id
                ));
            });
    }

    /**
     * Get Credit Card Fee by order.
     *
     * @param WC_Order $order Order object.
     * @param string $invoice_number
     *
     * @return WC_XR_ManualJournal
     */
    public function get_credit_card_fee_by_order($order, $invoice_number = '')
    {

        if ($invoice_number == '') {
            $invoice_number = $order->get_meta('_xero_invoice_number');
        }

        // Date time object of order data.
        $order_date = $order->get_date_created()->date('Y-m-d H:i:s');
        $date_parts = explode(' ', $order_date);
        $order_ymd = $date_parts[0];

        // The Payment object.
        $manual_journal = new WC_XR_ManualJournal();

        // Get payment gateway specific account codes
        list ($clearing_account, $fee_account) = $this->get_account_codes($order);

        // Credit clearing account.
        $manual_journal->set_credit_account_code($clearing_account);

        // Debit credit card fee account.
        $manual_journal->set_debit_account_code($fee_account);

        // Set the payment date.
        $manual_journal->set_date(apply_filters('woocommerce_xero_order_payment_date', $order_ymd, $order));

        // Set the amount.
        $manual_journal->set_amount($this->get_credit_card_fee($order));

        // Set narration/description
        $manual_journal->set_narration("Credit card fees for invoice: {$invoice_number}");

        return $manual_journal;
    }

    /**
     * @param WC_Order $order Order object.
     *
     * @return array
     */
    public function get_account_codes($order)
    {
        $payment_method = strtolower($order->get_payment_method());

        return array(
            $this->settings->get_option($payment_method . '_payment_account'),
            $this->settings->get_option($payment_method . '_fee_account')
        );
    }

    /**
     * @param WC_Order $order Order object.
     *
     * @return float
     */
    public function get_credit_card_fee($order)
    {
        $payment_method = strtolower($order->get_payment_method());
        switch ($payment_method) {
            case 'stripe' :
                return floatval(WC_Stripe_Helper::get_stripe_fee($order));
            case 'square_pos_card':
                return floatval($order->get_meta('square_processing_fee_money'));
            default:
                return 0;
        }
    }

    /**
     *
     * @param array $actions order actions array to display
     * @return array - updated actions
     */
    function add_order_action ( $actions ) {
        $actions['send_credit_card_fee_manually'] = __( 'Send Credit Card Fee to Xero', 'wc-xero' );
        return $actions;
    }

    public function send_credit_card_fee_manually ( $order )
    {
        $this->send_credit_card_fee($order->get_id());
    }
}
