<?php
/**
 * Xero Credit Card Fee Manager class.
 *
 * @package WC_Xero_Square_Extension
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-credit-card-fee-manager.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-scheduler.php';

/**
 * Xero Credit Card Fee Manager.
 */
class WC_XR_Schedule_Credit_Card_Fee_Manager extends WC_XR_Scheduler
{
    /**
     * Xero settings.
     *
     * @var WC_XR_Settings
     */
    private $settings;

    private $credit_card_fee_manager;

    /**
     * WC_XR_Payment_Manager constructor.
     *
     * @param WC_XR_Settings $settings Xero settings.
     */
    public function __construct(WC_XR_Settings $settings = null)
    {
        $this->settings = $settings;
        $this->setup_hooks();
        $this->credit_card_fee_manager = new WC_XR_Credit_Card_Fee_Manager($this->settings);
    }

    /**
     * Set up callbacks to the hooks.
     */
    private function setup_hooks()
    {
        add_action('send_credit_card_fee_action', array($this, 'send_credit_card_fee'), 10, 2);
    }

    public function schedule_send_credit_card_fee($order_id, $retry = 1)
    {
        $this->schedule_order_action('send_credit_card_fee_action', $order_id, 'Scheduled to create credit card fee.  ',
            $retry);
    }

    public function send_credit_card_fee($order_id)
    {
        $this->fire([$this->credit_card_fee_manager,'send_credit_card_fee'], $order_id);
    }

    public function no_invoice_number_failure($order_id)
    {
        $latest_notes = current(wc_get_order_notes(
            array(
                'order_id' => $order_id,
                'limit' => 1,
                'orderby' => 'date_created_gmt',
            )
        ));
        return preg_match("/No invoice number has not been sent/i", $latest_notes->content);
    }
}
