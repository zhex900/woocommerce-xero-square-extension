<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Testing
{
    function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_action( 'woocommerce_before_cart', array($this, 'woocommerce_clear_cart_url'),10 );
    }

    public function woocommerce_clear_cart_url() {
        if ( isset( $_GET['clear-cart'] ) ) {
            WC()->cart->empty_cart();
        }
    }
}
