<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WC_XR_Tax
{
    function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_filter('woocommerce_xero_line_item_tax_rate', array($this, 'get_tax_rate'));
	    add_filter( 'woocommerce_calc_tax', array( $this, 'adjust_tax' ), 1, 1 );
    }

	public function adjust_tax( $taxes ) {
		foreach ( $taxes as $i => $tax ) {
			$taxes[ $i ] = round( $taxes[$i], 2 );
		}
		return $taxes;
	}

    public function get_tax_rate()
    {
        return [
            'rate' => 10,
            'label' => 'GST'
        ];
    }
}
