<?php
use Biblys\Isbn\Isbn as Isbn;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 *
 * Add Google book preview button to the product page.
 *
 * The preview button will only show when the Google book
 * preview is available.
 *
 * Class WC_XR_Google_Books_Preview
 *
 */
class WC_XR_Google_Books_Preview
{

    function __construct()
    {
        $this->setup_hooks();
    }

    public function setup_hooks()
    {
        add_action( 'woocommerce_single_product_summary', array($this,'book_preview_button'), 30 );
    }


    public function book_preview_button() {
        global $product;

        // convert ISBN 13 to ISBN 10
        $isbn = new Isbn($product->get_sku());
        $isbn_10 = $isbn->format('ISBN-10');

        /**
         * Not all books exists in Google books.
         * However some books exists in Google books with
         * different ISBN. A possible reason is a book might
         * have different editions.
         *
         * For example, the Economy of God.
         * ISBN-13: 9780870834158
         * ISBN-10: 0870834150
         *
         * The same book in Google books have two
         * ISBNs 0736353917 and 0736331735
         *
         * A possible work around is to do a search through the api.
         * And find the a match with exact title, author and publisher.
         */

        if($this->isPreviewAvailable($isbn->format($isbn_10))){
            echo '<span id="__GBS_Button0" onclick="javascript:void window.open(\''.XERO_EXTENSION_PLUGIN_URL.'assets/html/googleBooksPreviewer.html?isbn='.$isbn_10.'\',\'1540768029335\',\'width=1000,height=1200,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=1\');return false;" style="cursor: pointer;"><img src="https://books.google.com/intl/en/googlebooks/images/gbs_preview_button1.gif" border="0" style="cursor: pointer;"></span>';
        }
    }

    /**
     *
     * Use the ISBN-10 to search for the book in Google books.
     * Return true if is found else false
     *
     * @param $isbn
     * @return bool
     *
     */
    public function isPreviewAvailable($isbn){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://books.google.com/books?bibkeys=".$isbn."&jscmd=viewapi",
            CURLOPT_USERAGENT => 'streampublications.com.au'
        ));

        $response_json = $this->convertResponseToJson(curl_exec($curl));
        curl_close($curl);

        if( !is_null($response_json) && property_exists($response_json,'preview') ){
            return $response_json->preview!='noview';
        }else{
            return false;
        }
    }

    /**
     *
     * Convert response into json
     * return the first element of the object array
     *
     * @param $response
     * @return object|null]
     *
     */
    private function convertResponseToJson($response){

        //extract the json string
        $pattern = '/({.*});$/m';
        preg_match($pattern, $response, $matches);

        if(sizeof($matches)>0) {
            $response_json = (Array) json_decode(($matches[1]));
            return array_pop($response_json);
        }else{
            return null;
        }
    }
}
