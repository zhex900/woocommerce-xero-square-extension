<?php
/**
 * Xero Credit Card Fee Manager class.
 *
 * @package WC_Xero_Square_Extension
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

require_once ABSPATH . 'wp-content/plugins/woocommerce-xero/includes/class-wc-xr-payment-manager.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-scheduler.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-schedule-credit-card-fee-manager.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-mail.php';

/**
 * Xero Credit Card Fee Manager.
 */
class WC_XR_Schedule_Payment_Manager extends WC_XR_Scheduler
{

    /**
     * Xero settings.
     *
     * @var WC_XR_Settings
     */
    private $settings;

    private $payment_manager;

    private $scheduler_credit_card_fee_manager;

    /**
     * WC_XR_Payment_Manager constructor.
     *
     * @param WC_XR_Settings $settings Xero settings.
     */
    public function __construct(WC_XR_Settings $settings = null)
    {
        if (!is_null($settings)) {
            $this->settings = $settings;
            $this->setup_hooks();
            $this->payment_manager = new WC_XR_Payment_Manager($this->settings);
            $this->scheduler_credit_card_fee_manager = new WC_XR_Schedule_Credit_Card_Fee_Manager($this->settings);
        }
    }

    /**
     * Set up callbacks to the hooks.
     */
    public function setup_hooks()
    {
        add_action('send_payment_action', array($this, 'send_payment'), 10, 2);
        add_action('wc_xero_send_payment', array($this, 'set_payment_account_codes'), 10, 1);
    }

    public function schedule_send_payment($order_id, $retry = 1)
    {
        // only send payment when payment method is not cod (invoice)
        $payment_method = $this->get_payment_method($order_id);
        if ($payment_method != 'cod') {
            $this->schedule_order_action('send_payment_action', $order_id,
                'Scheduled to create Xero Payment to Invoice.  ', $retry);
        } else {
            // cod means pay by invoice.
            // pay by invoice customers should receive the invoice after xero generated
            // the invoice
            WC_XR_Mail::send_invoice_to_customer($order_id);
        }
    }

    public function send_payment($order_id)
    {
        if ($this->fire([$this->payment_manager, 'send_payment'], $order_id)) {
            // send the invoice after the payment is updated in the xero invoice.
            WC_XR_Mail::send_invoice_to_customer($order_id);

            $payment_method = $this->get_payment_method($order_id);
            if ($payment_method != 'square_pos_cash') {
                $this->scheduler_credit_card_fee_manager->schedule_send_credit_card_fee($order_id);
            }
        }

    }

    public function set_payment_account_codes($order_id)
    {
        $payment_method = $this->get_payment_method($order_id);

        switch ($payment_method) {
            case 'square_pos_cash' :
                update_option('wc_xero_payment_account', get_option('wc_xero_square_pos_cash_payment_account'));
                break;
            case 'square_pos_card' :
                update_option('wc_xero_payment_account', get_option('wc_xero_square_pos_card_payment_account'));
                break;
            case 'stripe':
                update_option('wc_xero_payment_account', get_option('wc_xero_stripe_payment_account'));
            default:
                return;
        }
    }

    private function get_payment_method($order_id)
    {
        $order = wc_get_order($order_id);
        return strtolower($order->get_payment_method());
    }
}
