<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class WC_XR_Product {

	private $sales_account_code = '200';
	private $amount = 0;
	private $tax_code = 'OUTPUT';
    private $inventory_code = '';
    const MAX_LENGTH = 50;
    private $products;
    public function __construct(array $products)
    {
        $this->products=$products;
    }

    /**
     * @return string
     */
    public function get_name(WC_Product $product) {
        return str_replace('&', 'and', substr($product->get_name(),0,self::MAX_LENGTH-2));
    }

    /**
     * @return string
     */
    public function get_sales_account_code() {
        $code = $squord = get_option('square_order');
        return $this->sales_account_code;
    }

    /**
     * @param string $sales_account_code
     */
    public function set_sales_account_code( $sales_account_code ) {
        $this->sales_account_code = $sales_account_code;
    }

	/**
	 * @return int
	 */
	public function get_amount() {
		return $this->amount;
	}

	/**
	 * @param int $amount
	 */
	public function set_amount( $amount ) {
		$this->amount = floatval( $amount );
	}

	public function set_inventory_code( $inventory_code ){
	    $this->inventory_code = $inventory_code;
    }

    public function get_inventory_code(WC_Product $product){
	    return $product->get_sku();
    }

    public function set_tax_code( $tax_code ){
        $this->tax_code = $tax_code;
    }

    public function get_tax_code(){
        return $this->tax_code;
    }
	/**
	 * Return XML of Manual Journal
	 * Debits are positive, credits are negative value
	 * @return string
	 */
	public function to_xml() {
        $xml  = '<Items>'.$this->products_to_xml().'</Items> ';
		return $xml;
	}

	private function products_to_xml(){
	    $xml = '';
	    foreach($this->products as $product){
	        $xml .= $this->product_to_xml($product);
        }
        return $xml;
    }

	private function product_to_xml(WC_Product $product){
        $xml= '';
	    if( $product->get_name() != '' && $product->get_sku() != '') {
            $xml = '
                <Item>'.$this->get_xero_inventory_id($product).'
                    <Code>' . $this->get_inventory_code($product) . '</Code>
                    <Description>'.$product->get_id().'</Description>
                    <Name>' . $this->get_name($product) . '</Name>
                    <SalesDetails>
                      <UnitPrice>' . $product->get_regular_price() . '</UnitPrice>
                      <TaxType>' . $this->get_tax_code() . '</TaxType>
                      <AccountCode>' . $this->get_sales_account_code() . '</AccountCode>
                    </SalesDetails>
                 </Item>';
        }
        return $xml;
    }

    private function get_xero_inventory_id(WC_Product $product){
        $xero_inventory_id = get_post_meta( $product->get_id(), '_xero_inventory_id', true );
        if( $xero_inventory_id != '' ){
            return '<ItemID>'.$xero_inventory_id.'</ItemID>';
        }
        return '';
    }
}
