function sync_ajax_pop_up($message, $ajax) {
    const r = confirm($message);
    if (r == true) {
        jQuery.ajax({
            type: "GET",
            url: ajaxurl,
            data: `action=${$ajax}`,
            success: function (response) {
                console.log(response);
                alert(response);
            }
        });
    }
}

jQuery(document).ready(function () {
    jQuery("#wc_xero_sync_xero2woo_contact").click(function () {
        sync_ajax_pop_up(
            'Are you sure you want to sync Xero customers to WooCommerce?',
            'sync_xero_to_woo_customer'
        )
    });

    jQuery("#wc_xero_sync_square2woo_contact").click(function () {
        sync_ajax_pop_up(
            'Are you sure you want to sync Square customers to WooCommerce?',
            'sync_square_to_woo_customer'
        )
    });

    jQuery("#wc_xero_sync_woo2square_contact").click(function () {
        sync_ajax_pop_up(
            'Are you sure you want to sync WooCommerce customers to Square?',
            'sync_woo_to_square_customer'
        )
    });

    jQuery("#wc_xero_sync_woo2xero_product").click(function () {
        sync_ajax_pop_up(
            'Are you sure you want to sync WooCommerce products to Xero?',
            'sync_woo_to_xero_product'
        )
    });

    jQuery("#wc_xero_sync_woo2xero_account_codes").click(function () {
        sync_ajax_pop_up(
            'Are you sure you want to sync WooCommerce account codes to Xero?',
            'sync_woo_to_xero_account_codes'
        )
    });
});

