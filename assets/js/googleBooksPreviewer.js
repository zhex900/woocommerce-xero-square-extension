
function showBookPreview() {
    const url = new URL(location);
    const isbn = url.searchParams.get("isbn");
    GBS_insertEmbeddedViewer(`ISBN:${isbn}`, 800, 1800);
}

/**
 * Convert ISBN 13 to ISBN 10
 * https://codegolf.stackexchange.com/questions/153624/converting-isbn-13-to-isbn-10
 * @param s
 * @returns {*}
 */
convertISBN13=s=>(s=s.slice(3,-1))+[...s].reduce(n=>n+s[i++]*i,i=0)%11;