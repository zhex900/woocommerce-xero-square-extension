<?php
/**
 * Plugin Name: WooCommerce Xero and Square Integration Extension
 * Plugin URI:
 * Description: Woocommerce to Xero and Square integration: invoice, customer, payment and inventory .
 * Version: 0.1
 * Author: Jake He
 * Author URI:
 * License: MIT
 * License URI: http://www.opensource.org/licenses/gpl-license.php
 * WC requires at least: 3.4.0
 * WC tested up to: 3.4.0
 * Text Domain: woocommerce-xero-square-extension
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

require_once ABSPATH . 'wp-content/plugins/woocommerce-xero/includes/class-wc-xr-oauth20.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero/includes/class-wc-xr-settings.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/vendor/autoload.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-get-invoice.php';
require_once ABSPATH . 'wp-content/plugins/woocommerce-xero-square-extension/includes/class-wc-xr-myaccount.php';
require_once ABSPATH . 'wp-admin/includes/template.php';

define('XERO_EXTENSION_PLUGIN_URL', plugin_dir_url(__FILE__));

if (!class_exists('WC_Xero_Square_Extension')) :

    class WC_Xero_Square_Extension
    {

        const VERSION = '0.1';
        const OPTION_PREFIX = 'wc_xero_';

        protected static $_instance = null;
        protected $settings = array();
        protected $xero_settings = null;

        /**
         * Main Plugin Instance
         *
         * Ensures only one instance of plugin is loaded or can be loaded.
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }

        /**
         * Constructor
         */
        public function __construct()
        {
            // force the timezone to UTC otherwise woocommerce will not use the wordpress timezpone.
            date_default_timezone_set('UTC');
            // Set the settings
            $this->settings = array(
                'square_access_token' => array(
                    'title' => __('Square Access Token', 'wc-xero'),
                    'default' => '',
                    'type' => 'text',
                    'description' => __('Square Access Token', 'wc-xero'),
                ),
                'stripe_payment_account' => array(
                    'title' => __('Stripe Clearing Account', 'wc-xero'),
                    'default' => '',
                    'type' => 'text',
                    'description' => __('Code for Xero account to track payments received.', 'wc-xero'),
                ),
                'stripe_fee_account' => array(
                    'title' => __('Stripe Fee Account', 'wc-xero'),
                    'default' => '',
                    'type' => 'text',
                    'description' => __('Code for Xero account to track payments received.', 'wc-xero'),
                ),
                'square_pos_cash_payment_account' => array(
                    'title' => __('Square POS Cash Clearing Account', 'wc-xero'),
                    'default' => '',
                    'type' => 'text',
                    'description' => __('Code for Xero account to track payments received.', 'wc-xero'),
                ),
                'square_pos_card_payment_account' => array(
                    'title' => __('Square POS Card Clearing Account', 'wc-xero'),
                    'default' => '',
                    'type' => 'text',
                    'description' => __('Code for Xero account to track payments received.', 'wc-xero'),
                ),
                'square_pos_card_fee_account' => array(
                    'title' => __('Square POS Card Fee Account', 'wc-xero'),
                    'default' => '',
                    'type' => 'text',
                    'description' => __('Code for Xero account to track payments received.', 'wc-xero'),
                ),
                'sync_xero2woo_contact' => array(
                    'title' => __('Sync Xero customers to WooCommerce', 'wc-xero'),
                    'default' => '',
                    'type' => 'button',
                    'description' => __('Sync Xero customers to WooCommerce.', 'wc-xero'),
                ),
                'sync_square2woo_contact' => array(
                    'title' => __('Sync Square customers to WooCommerce', 'wc-xero'),
                    'default' => '',
                    'type' => 'button',
                    'description' => __('Sync Square customers to WooCommerce.', 'wc-xero'),
                ),
                'sync_woo2square_contact' => array(
                    'title' => __('Sync WooCommerce customers to Square', 'wc-xero'),
                    'default' => '',
                    'type' => 'button',
                    'description' => __('Sync WooCommerce customers to Square.', 'wc-xero'),
                ),
                'sync_woo2xero_product' => array(
                    'title' => __('Sync WooCommerce products to Xero', 'wc-xero'),
                    'default' => '',
                    'type' => 'button',
                    'description' => __('Sync WooCommerce products to Xero.', 'wc-xero'),
                ),
                'sync_woo2xero_account_codes' => array(
                    'title' => __('Sync WooCommerce account codes to Xero', 'wc-xero'),
                    'default' => '',
                    'type' => 'button',
                    'description' => __('Sync WooCommerce account codes to Xero.', 'wc-xero'),
                ),
                'view' => array(
                    'title' => __('', 'wc-xero'),
                    'default' => '',
                    'type' => 'view',
                    'description' => __('', 'wc-xero'),
                )
            );

            $this->plugin_basename = plugin_basename(__FILE__);
            $this->define('WC_Xero_Square_Extension_VERSION', self::VERSION);

            add_filter('rest_api_init', array($this, 'add_cors_support'));
            add_action('admin_enqueue_scripts', array($this, 'include_script'));
            add_action('admin_enqueue_scripts', array($this, 'include_style'));
            add_action('plugins_loaded', array($this, 'load_classes'), 9);
            add_action('wp_footer', array($this, 'tidiochat'));
            add_filter('s3_uploads_bucket_url', array($this, 'redirectToCDN'));
            add_filter('action_scheduler_queue_runner_concurrent_batches',
                array($this, 'action_scheduler_concurrent_batches'));
            add_filter('action_scheduler_queue_runner_time_limit', array($this, 'increase_time_limit'));
            add_action('init', [$this, 'disable_default_runner'], 10);
        }

        function disable_default_runner()
        {
            if (class_exists('ActionScheduler')) {
                remove_action('action_scheduler_run_queue', array(ActionScheduler::runner(), 'run'));
            }
        }

        function increase_time_limit()
        {
            return 120;
        }

        function action_scheduler_concurrent_batches()
        {
            return 3;
        }

        public function add_cors_support()
        {
            $headers = apply_filters('jwt_auth_cors_allow_headers',
                'Access-Control-Allow-Headers, Content-Type, Authentication, Authorization');
            header(sprintf('Access-Control-Allow-Headers: %s', $headers));
        }

        public function redirectToCDN($url)
        {
            $wp_rocket_settings = get_option('wp_rocket_settings');

            return 'https://' . $wp_rocket_settings['cdn_cnames'][0]; //'http://localhost:4572/stream-book-shop-bucket';
        }

        public function tidiochat()
        {
            ?>
            <script src="//code.tidio.co/24rxwgr5yz1gbqrzm7fp3nl5hx2chowp.js"></script>
            <?php
        }

        /**
         * include script
         */
        public function include_script()
        {
            wp_localize_script(
                'xero_extension_script',
                'ajax_obj',
                array('ajaxurl' => admin_url('admin-ajax.php'))
            );
            wp_enqueue_script('xero_extension_script', XERO_EXTENSION_PLUGIN_URL . 'assets/js/script.js',
                array('jquery'));
        }

        public function include_style()
        {
            wp_enqueue_style('xero_extension_script', XERO_EXTENSION_PLUGIN_URL . 'assets/css/style.css');
        }

        /**
         * Define constant if not already set
         *
         * @param string $name
         * @param string|bool $value
         */
        private function define($name, $value)
        {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        /**
         * Load the main plugin classes and functions
         */
        public function setup()
        {
            $this->register_settings();
            // Setup the autoloader.
            $this->setup_autoloader();

            // Setup Settings.
            $this->xero_settings = new WC_XR_Settings();
            new \Xero_Extension\WC_XR_Get_Invoice();
            new WC_XR_Default_Product_Weight_Dimension();
            new WC_XR_Attach_Invoice_To_Email();
            new WC_XR_My_Account();
            new WC_XR_Admin_Orders_Page();
            new WC_XR_Credit_Card_Fee_Manager($this->xero_settings);
            new WC_XR_Mail();
            new WC_XR_Shipping();
            new WC_XR_Square_Order();
            new WC_XR_Refund_Manager($this->xero_settings);
            new WC_XR_Sync_Customer_Xero_To_Woo($this->xero_settings);
            new WC_XR_Sync_Customer_Square($this->xero_settings);
            new WC_XR_Sync_Product_Woo_To_Xero($this->xero_settings);
            new WC_XR_Sync_Account_Code_Woo_To_Xero($this->xero_settings);
            new WC_XR_Sync_Product_Square();
            new WC_XR_Google_Books_Preview();
            new WC_XR_Testing();
            new WC_XR_Schedule_Invoice_Manager($this->xero_settings);
            new WC_XR_Schedule_Payment_Manager($this->xero_settings);
            new WC_XR_Invoice_Reference();
            new WC_XR_Tax();
            new WC_XR_Correct_Order_Rounding_Error();
            new WC_XR_User_Registration();
            new WC_XR_API_Xero_Oauth2();
        }

        /**
         * settings_init()
         *
         * @access public
         * @return void
         */
        public function register_settings()
        {

            // Add section
            add_settings_section('wc_xero_settings', __('Xero Settings', 'wc-xero'), array(
                $this,
                'settings_intro'
            ), 'woocommerce_xero');

            // Add setting fields
            foreach ($this->settings as $key => $option) {

                // Add setting fields
                add_settings_field(self::OPTION_PREFIX . $key, $option['title'], array(
                    $this,
                    'input_' . $option['type']
                ), 'woocommerce_xero', 'wc_xero_settings', array('key' => $key, 'option' => $option));

                if ('key_file' === $option['type']) {
                    add_filter('pre_update_option_' . self::OPTION_PREFIX . $key,
                        array($this, 'handle_key_file_upload'), 10, 3);
                }

                // Register setting
                register_setting('woocommerce_xero', self::OPTION_PREFIX . $key);

            }

        }

        /**
         * Text setting field
         *
         * @param array $args
         */
        public function input_text($args)
        {
            echo '<input type="text" name="' . self::OPTION_PREFIX . $args['key'] . '" id="' . self::OPTION_PREFIX . $args['key'] . '" value="' . $this->xero_settings->get_option($args['key']) . '" />';
            echo '<p class="description">' . $args['option']['description'] . '</p>';
        }

        public function input_view($args)
        {
            echo '
                <div id="cover-spin">
                </div>
            ';
        }

        public function input_button($args)
        {
            echo '<input type="button" class="button-primary" name="' . self::OPTION_PREFIX . $args['key'] . '" id="' . self::OPTION_PREFIX . $args['key'] . '" value="' . $args['option']['description'] . '" />';
            echo '<p class="description">' . $args['option']['description'] . '</p>';
        }

        /**
         * A static method that will setup the autoloader
         *
         * @static
         * @since  1.0.0
         * @access private
         */
        private function setup_autoloader()
        {
            require_once(plugin_dir_path(self::get_plugin_file()) . '../woocommerce-xero/includes/class-wc-xr-autoloader.php');

            // Core loader.
            $autoloader = new WC_XR_Autoloader(plugin_dir_path(self::get_plugin_file()) . 'includes/');
            spl_autoload_register(array($autoloader, 'load'));
        }

        /**
         * Get the plugin file.
         *
         * @static
         * @return String
         * @since  1.0.0
         * @access public
         *
         */
        public static function get_plugin_file()
        {
            return __FILE__;
        }

        /**
         * Instantiate classes when woocommerce is activated
         */
        public function load_classes()
        {
            if ($this->is_woocommerce_activated() === false) {
                add_action('admin_notices', array($this, 'need_woocommerce'));

                return;
            }

            if (version_compare(PHP_VERSION, '5.3', '<')) {
                add_action('admin_notices', array($this, 'required_php_version'));

                return;
            }

            // all systems ready - GO!
            $this->setup();
        }

        /**
         * Check if woocommerce is activated
         */
        public function is_woocommerce_activated()
        {
            $blog_plugins = get_option('active_plugins', array());
            $site_plugins = is_multisite() ? (array)maybe_unserialize(get_site_option('active_sitewide_plugins')) : array();

            if (in_array('woocommerce/woocommerce.php',
                    $blog_plugins) || isset($site_plugins['woocommerce/woocommerce.php'])) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * WooCommerce not active notice.
         *
         * @return string Fallack notice.
         */

        public function need_woocommerce()
        {
            $error = sprintf(__('woocommerce-xero-square-extension requires %sWooCommerce%s to be installed & activated!',
                'woocommerce-xero-square-extension'), '<a href="http://wordpress.org/extend/plugins/woocommerce/">',
                '</a>');

            $message = '<div class="error"><p>' . $error . '</p></div>';

            echo $message;
        }

        /**
         * PHP version requirement notice
         */

        public function required_php_version()
        {
            $error = __('WooCommerce WC Xero Extension requires PHP 5.3 or higher (5.6 or higher recommended).',
                'woocommerce-xero-square-extension');
            $how_to_update = __('How to update your PHP version', 'woocommerce-xero-square_extension');
            $message = sprintf('<div class="error"><p>%s</p><p><a href="%s">%s</a></p></div>', $error,
                'http://docs.wpovernight.com/general/how-to-update-your-php-version/', $how_to_update);

            echo $message;
        }


        /**
         * Get the plugin url.
         * @return string
         */
        public function plugin_url()
        {
            return untrailingslashit(plugins_url('/', __FILE__));
        }

        /**
         * Get the plugin path.
         * @return string
         */
        public function plugin_path()
        {
            return untrailingslashit(plugin_dir_path(__FILE__));
        }

    }
//
endif; // class_exists

/**
 * Returns the main instance of woocommerce-xero-square_extension
 *
 * @return WC_Xero_Square_Extension
 */
function WC_Xero_Square_Extension()
{
    return WC_Xero_Square_Extension::instance();
}

WC_Xero_Square_Extension(); // load plugin
