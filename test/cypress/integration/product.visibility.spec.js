/// <reference types="Cypress" />
import data from '../fixtures/product-visibility.json'

data.forEach((current, i) => {
    describe(`${i + 1} of ${data.length}, product-visibility: ${current.user}`, () => {

        beforeEach(() => {
            cy.clearCookies()
            cy.login(current.user, current.pass)
        })

        it(`Catalog: ${current.catalog}`, () => {
            cy.visit(`/product-category/books/${current.catalog}`);

            // loop through each product
            cy.get('[data-product_id]').each(($product) => {
                let productId = $product.attr('data-product_id')
                cy.log(productId)
                cy.log($product.attr('aria-label'))

                const ADD_TO_CART = "add_to_cart_button"
                // fetch the product prices from DB
                cy.getProductPrices(productId).then(prices => {
                    // if product have retail price, it is available for all consumer
                    if (Object.keys(prices).includes('retail')) {
                        expect($product.attr('class')).to.contain(ADD_TO_CART)
                    } else {
                        // member exclusive products
                        // exclusive these customers
                        if (['retail', 'wholesale'].includes(current.type)) {
                            // exclude member only products
                            expect($product.attr('class')).to.not.contain(ADD_TO_CART)
                        } else {
                            expect($product.attr('class')).to.contain(ADD_TO_CART)
                        }
                    }
                })
            });
        });
    })
})