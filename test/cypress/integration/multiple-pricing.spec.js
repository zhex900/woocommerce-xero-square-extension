/// <reference types="Cypress" />
import data from '../fixtures/multiple-pricing.json'

data.forEach((current, i) => {
    describe(`${i + 1} of ${data.length}, multiple-pricing: ${current.user}`, () => {

        beforeEach(() => {
            cy.clearCookies()
            cy.login(current.user, current.pass)
        })
        // three types member, retail and wholesale
        // only two are displayed, member and retail or member and wholesale
        let priceTypes = ['member']
        if (current.type === 'wholesale') {
            priceTypes.push('wholesale')
        } else {
            priceTypes.push('retail')
        }

        it('Featured product prices', () => {
            cy.visit('/');

            // loop through each product
            cy.get('.product-item').each($product => {
                // get product id from the wishlist
                let wishlist = $product.find('.single_add_to_wishlist').attr('href');
                // find the prices within the product div
                cy.wrap($product).within(() => {
                    // get the product id from href
                    let productId = wishlist.split('=')[1];
                    cy.get('.price').verifyPrice(productId, (prices) => {
                        // only two prices are displayed
                        prices.map((price) => {
                            expect(Object.keys(price)[0]).to.be.oneOf(priceTypes)
                        })
                    })
                });
            });
        });


        it('Check cart price', () => {
            cy.visit('/cart/?clear-cart') // clear cart
            cy.visit(`/product/${current.book}`)

            // get product id
            cy.get('[data-product-id]').then(($element) => {
                let productId = $element.attr('data-product-id')
                cy.log(productId)
                cy.get('.summary > .price').verifyPrice(productId, (prices) => {
                    // check the cart price remains as retail price
                    cy.get('button[name=add-to-cart]').click()
                    cy.visit('/cart')
                    let price = prices.filter(x => x.hasOwnProperty(current.type))[0][current.type]
                    cy.get('.cart-subtotal > td > .woocommerce-Price-amount').contains(price)
                })
            })
        })
    })
})