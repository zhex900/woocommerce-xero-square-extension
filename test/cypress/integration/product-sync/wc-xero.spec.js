/// <reference types="Cypress" />
import {
  transformObjectArray,
  assertObjectsEqual,
  trimBookTitle
} from '../../support/tools'

describe('WC to Xero product sync', () => {

  // beforeEach(() => {
  //   cy.clearCookies()
  //   cy.fixture('users').then(users => {
  //     cy.login(users.admin.username, users.admin.password)
  //   })
  // })

  it('All products', () => {
    // delete all xero products
    // delete all WC product xero inventory reference
    // cy.deleteWCProductXeroLink()



    // cy.visit('/wp-admin/admin.php?page=woocommerce_xero');
    // cy.get('#wc_xero_sync_woo2xero_product').click().then(() => {
    //   // get all WC products
    cy.getWCProducts().then(products => {

      let wc_products = transformObjectArray(products, (element) => {
        return {
          [element.ItemID]: {
            Code: parseInt(element.Code),
            Description: parseInt(element.Description),
            Name: trimBookTitle(element.Name),
            UnitPrice: parseFloat(element.UnitPrice)
          }
        }
      })

      cy.task('getXeroProducts').then(result => {
        let xero_products = transformObjectArray(result.Items, (element) => {
          return {
            [element.ItemID]: {
              Code: parseInt(element.Code),
              Description: parseInt(element.Description),
              Name: trimBookTitle(element.Name),
              UnitPrice: parseFloat(element.SalesDetails.UnitPrice)
            }
          }
        })
        assertObjectsEqual(wc_products, xero_products)
      })
    })
  })
  // })
})