/// <reference types="Cypress" />
import data from '../fixtures/payment-options.json'
import faker from 'faker'

data.forEach((current, i) => {
    describe(`${i + 1} of ${data.length}, payment-options: ${current.user}`, () => {

        beforeEach(() => {
            cy.clearCookies()
            cy.login(current.user, current.pass)
        })
        let allowedPaymentMethods = ['stripe', 'alipay']
        if (['church', 'wholesale'].includes(current.type)) {
            allowedPaymentMethods.push('cod') // cod is pay by invoice
        }

        it('Payment options', () => {

            cy.getAvailableWCProductsByUserRole(current.type).then(products => {
                let randomProduct = faker.random.objectElement(products)
                console.log(randomProduct)
                cy.visit(`/product/${randomProduct.slug}`)
                // add product to cart
                cy.get('button[name=add-to-cart]').click()
                cy.visit('/cart')
                cy.get('.checkout-button').click()
                cy.get('.wc_payment_method').each($payment_methods => {
                    let paymentMethod = $payment_methods.context.className.split('_').pop()
                    expect(allowedPaymentMethods).includes(paymentMethod)
                })
            })

        });
    })
})