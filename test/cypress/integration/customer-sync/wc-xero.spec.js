// /// <reference types="Cypress" />
// import {
//   transformObjectArray,
//   assertObjectsEqual
// } from '../../support/tools'

// describe('WC to Xero customer sync', () => {

//   // beforeEach(() => {
//   //   cy.clearCookies()
//   //   cy.fixture('users').then(users => {
//   //     cy.login(users.admin.username, users.admin.password)
//   //   })
//   // })

//   it('All customers', () => {
//     // delete all xero products
//     // delete all WC product xero inventory reference
//     // cy.deleteWCProductXeroLink()

//     const NAME_SIZE = 50 - 2

//     // cy.visit('/wp-admin/admin.php?page=woocommerce_xero');
//     // cy.get('#wc_xero_sync_woo2xero_product').click().then(() => {
//     //   // get all WC products
//     cy.getWCCustomers().then(customers => {

//       let wc_products = transformObjectArray(products, (element) => {
//         return {
//           [element.ItemID]: {
//             Code: parseInt(element.Code),
//             Description: parseInt(element.Description),
//             Name: element.Name.substring(0, NAME_SIZE).replace(/\&/g, 'and'),
//             UnitPrice: parseFloat(element.UnitPrice)
//           }
//         }
//       })

//       cy.task('getXeroCustomers').then(result => {
//         let xero_products = transformObjectArray(result.Items, (element) => {
//           return {
//             [element.ItemID]: {
//               Code: parseInt(element.Code),
//               Description: parseInt(element.Description),
//               Name: element.Name.substring(0, NAME_SIZE).replace(/\&/g, 'and'),
//               UnitPrice: parseFloat(element.SalesDetails.UnitPrice)
//             }
//           }
//         })
//         assertObjectsEqual(wc_products, xero_products)
//       })
//     })
//   })
//   // })
// })