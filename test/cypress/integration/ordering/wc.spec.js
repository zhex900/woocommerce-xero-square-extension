/// <reference types="Cypress" />
import data from '../../fixtures/ordering.json';
import Order from '../../page-objects/order';
import faker from 'faker';

data.forEach((current, i) => {
  describe(`${i + 1} of ${data.length}, ordering: ${current.user}`, () => {
    beforeEach(() => {
      cy.fixture('users').as('users')
      cy.clearCookies();
      cy.login(current.user, current.pass);
    });
    let allowedPaymentMethods = ['stripe', 'alipay'];
    if (['church', 'wholesale'].includes(current.type)) {
      allowedPaymentMethods.push('cod'); // cod is pay by invoice
    }
    let order = new Order();

    it('Order test', () => {
      cy.getAvailableWCProductsByUserRole(current.type).then(products => {
        order.addRandomProducts(products, 1)

        cy.visit('/cart');

        // select shipping
        const shippingFlatRate = '#shipping_method_0_flat_rate1';
        const shippingLocalPick = '#shipping_method_0_local_pickup3';
        cy.get(shippingFlatRate).check();

        order.checkOut();

        cy.fillFields(order.fields.billing);
        cy.fillFields(order.fields.billing.address);

        cy.log(JSON.stringify(order.fields.products));

        // select payment
        cy.get('#payment_method_stripe').check()
        // place order
        order.placeOrder()

        cy.get('.wc-stripe-checkout-button').click({
          force: true
        });
        // enter credit card details.
        order.stripePay()

        // get the order id
        cy.url().then(url => {
          let orderId = url.match(/order-received\/(\d*)/)[1]
          cy.log('Order id: ' + orderId)
          cy.getInvoiceNumberByWCOrderID(orderId)
            .then(invoiceNumber => {
              cy.log(`Invoice# ${invoiceNumber}`)

              // logout
              cy.logout()
              cy.get('@users')
                .then((users) => {
                  cy.login(users.admin.username, users.admin.password, true)
                })
              cy.visit(`/wp-admin/post.php?post=${orderId}&action=edit`)
              cy.get('#xero-invoice-number').parent().should('contain', 'PAID')
              cy.get('#xero-invoice-number').should('contain', invoiceNumber)

              // verify xero invoice details
              order.verifyXeroInvoice(orderId, invoiceNumber)

              // check customer name
              // line items
              // total amount
              // GST

            })
        })

        // cy.log('Order# ' + order.fields.orderId)


        //
        // verify xero credit card fee

        // verify admin order page
        // invoice is paid.
        // check invoice#

        // if existing user check my account

      });
    });
  });
});