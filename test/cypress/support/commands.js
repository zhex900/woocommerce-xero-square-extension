// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import isEmpty from 'lodash/isEmpty.js'
import {
  transformObjectArray
} from './tools'

Cypress.Commands.add('getProductPrices', (productId) => {
  cy.log('Get product prices from DB')
  const sql = `SELECT * FROM wp_wusp_role_pricing_mapping where product_id="${productId}"`
  return cy.task('query', sql).then((result) => {
    let prices = {}
    result.map((item) => {
      return {
        [item.role]: item.price
      }
    }).forEach(price => {
      prices = { ...prices,
        ...price
      }
    })
    return prices
  })
})

Cypress.Commands.add('getInvoiceNumberByWCOrderID', (orderId) => {
  cy.log('Get Xero invoice number')
  const sql = `SELECT * FROM wp_postmeta WHERE post_id ="${orderId}" AND meta_key ="_xero_invoice_number"`
  return cy.task('query', sql).then((result) => {
    console.log(result)
    return result[0].meta_value
  })
})


Cypress.Commands.add('getWCProducts', () => {
  cy.log('Get all products from DB')
  const sql = `
SELECT wp_posts.ID as Description, post_title as Name, m.meta_value as Code, p.price as UnitPrice, m1.meta_value as ItemID FROM wp_posts
LEFT JOIN wp_postmeta m ON wp_posts.ID = m.post_id
LEFT JOIN wp_postmeta m1 ON wp_posts.ID = m1.post_id
LEFT JOIN wp_wusp_role_pricing_mapping p ON p.product_id = m.post_id
WHERE m.meta_key = "_sku"
AND p.role = "member"
AND post_status = "publish"
AND m1.meta_key = "_xero_inventory_id"
GROUP BY wp_posts.ID, post_title, m.meta_value, p.price, m1.meta_value `
  return cy.task('query', sql).then((result) => {
    return result
  })
})

/**
 * Get all the available WC products per user role
 * member = church,
 * retail = wholesale
 */
Cypress.Commands.add('getAvailableWCProductsByUserRole', (role) => {
  role = role.replace(/church/g, 'member').replace(/wholesale/g, 'retail')
  const sql = `SELECT wp_posts.ID as id, post_title as title, post_name as slug FROM
wp_posts  LEFT JOIN wp_wusp_role_pricing_mapping p ON p.product_id = wp_posts.ID
WHERE p.role = "${role}" AND post_status ='publish' AND post_type='product' GROUP BY  wp_posts.ID, post_title, post_name`
  //@todo add role prices
  return cy.task('query', sql).then((result) => {
    return transformObjectArray(result, (element) => {
      return {
        [element.id]: {
          id: element.id,
          slug: element.slug,
          title: element.title
        }
      }
    })
  })
})

Cypress.Commands.add('deleteWCProductXeroLink', () => {
  const sql = 'DELETE FROM wp_postmeta WHERE meta_key = "_xero_inventory_id"'
  return cy.task('query', sql).then((result) => {
    return result
  })
})

Cypress.Commands.add('verifyPrice', {
  prevSubject: true,
}, (subject, productId, callback) => {
  return cy.wrap(subject).then(($price) => {
    /**
     * transform "$30.80 Guest price$24.50 Member price"
     * into [{retail:30.80},{member:24.50}]
     */
    let prices = $price[0].textContent.replace(/price|\$/g, '').trim().split(' ')
    let type = prices.filter(x => isNaN(parseFloat(x)))
    let amount = prices.filter(x => !isNaN(parseFloat(x)))
    prices = type.map((x, i) => {
      return {
        [x.toLowerCase().replace(/guest/g, 'retail')]: parseFloat(amount[i])
      }
    })
    cy.log(JSON.stringify(prices))

    // fetch the product prices from DB
    cy.getProductPrices(productId).then(dbPrices => {
      // dbPrices is an object {retail:11.0,church:22.0}
      cy.log(JSON.stringify(dbPrices)) // debug log
      prices.forEach(price => {
        // check the prices
        let priceType = Object.keys(price)[0]
        expect(dbPrices[priceType]).to.eql(price[priceType])
      })

    })
    callback(prices)
  })
})

Cypress.Commands.add("login", (username, password, isAdmin = false) => {
  if (username !== '') {
    cy.visit('/wp-login.php')
    cy.get('#user_login').clear()
    cy.get('#user_login').type(username);
    cy.get('#user_pass').type(password);
    cy.get('#wp-submit').click();
    if (!isAdmin) {
      cy.get('.login-register-wrap').then(($login) => {
        expect($login[0].innerText).to.contain('Logout')
      })
    } else {
      cy.url().should('include', '/wp-admin')
    }
  }
})

Cypress.Commands.add("logout", () => {
  cy.visit('/wp-login.php?action=logout', {
    failOnStatusCode: false
  })
  cy.get('a[href]').click({
    force: true
  })
  cy.url().should('include', 'loggedout=true')
  cy.visit('/')
})

Cypress.Commands.add('selectRandom', {
  prevSubject: true,
}, (subject, element) => {

  return cy.wrap(subject).grabSelectOptions().then(($options) => {
    const option = faker.random.arrayElement($options)
    cy.wrap(subject).select(option)
    cy.log(`Random select: [${option}]`)
    if (element !== undefined) {
      element.value = option
    }
  })
})

Cypress.Commands.add("fillFields", (fields) => {

  Object.values(fields).map(field => {
    if (!isEmpty(field.value)) {

      if (field.select === undefined) {
        cy.log(`Fill ${field.css} with value ${field.value}`)
        cy.get(field.css).clear()
        cy.get(field.css).type(field.value)
      } else {
        // cy.log(`Select ${field.select.css} with value ${field.value}`)
        cy.get(field.select.css).selectRandom()
      }
    }
  })
})

/**
 * returns an array of select values
 *
 * @param {string} subject
 * @param {array} exclude
 *
 */
Cypress.Commands.add('grabSelectOptions', {
  prevSubject: true
}, (subject, exclude = SELECT_EXCLUSION) => {
  cy.wrap(subject).within(() => {
    cy.get('option').then(($options) => {
      return $options.map((index, option) => {
        if (!exclude.includes(option.text)) {
          return option.text
        }
      })
    })
  })
})

Cypress.Commands.add('getOrderLineItems', (orderId) => {
  const sql = `
     SELECT
     product_id.meta_value AS productId,
       sku.meta_value as sku,
       o.order_item_name AS title,
       qty.meta_value AS qty,
       tax.meta_value As tax,
       round(line_total.meta_value, 2) as total
     FROM wp_woocommerce_order_items as o
     INNER JOIN wp_woocommerce_order_itemmeta AS line_total ON line_total.order_item_id = o.order_item_id
     INNER JOIN wp_woocommerce_order_itemmeta AS product_id ON product_id.order_item_id = o.order_item_id
     INNER JOIN wp_woocommerce_order_itemmeta AS tax ON tax.order_item_id = o.order_item_id
     INNER JOIN wp_woocommerce_order_itemmeta AS qty ON qty.order_item_id = o.order_item_id
     INNER JOIN wp_postmeta AS sku ON sku.post_id = product_id.meta_value
     WHERE o.order_id = ${orderId}
     AND line_total.meta_key = '_line_total'
     AND tax.meta_key = '_line_tax'
     AND product_id.meta_key = '_product_id'
     AND qty.meta_key = '_qty'
     AND sku.meta_key = '_sku'
     `
  return cy.task('query', sql).then(lineItems => {
    return transformObjectArray(lineItems, (item) => {
      return {
        [item.sku]: item
      }
    })
  })
})

Cypress.Commands.add('getOrderDetails', (orderId) => {
  const sql = `
        SELECT DISTINCT
        p.post_id AS orderId,
          firstname.meta_value AS firstName,
          lastname.meta_value AS lastName,
          email.meta_value AS email,
          payment_method.meta_value AS paymentMethod,
          xero_journal.meta_value AS xeroJournalId,
          round(tax.meta_value, 2) AS tax,
          round(total.meta_value, 2) AS total
        FROM wp_postmeta AS p
        INNER JOIN wp_postmeta AS firstname ON firstname.post_id = p.post_id
        INNER JOIN wp_postmeta AS lastname ON lastname.post_id = p.post_id
        INNER JOIN wp_postmeta AS email ON email.post_id = p.post_id
        INNER JOIN wp_postmeta AS payment_method ON payment_method.post_id = p.post_id
        INNER JOIN wp_postmeta AS xero_journal ON xero_journal.post_id = p.post_id
        INNER JOIN wp_postmeta AS tax ON tax.post_id = p.post_id
        INNER JOIN wp_postmeta AS total ON total.post_id = p.post_id
        WHERE p.post_id = ${orderId}
        AND firstname.meta_key = '_billing_first_name'
        AND lastname.meta_key = '_billing_last_name'
        AND email.meta_key = '_billing_email'
        AND payment_method.meta_key = '_payment_method'
        AND xero_journal.meta_key = '_xero_manual_journal_credit_card_fee_id'
        AND tax.meta_key = '_order_tax'
        AND total.meta_key = '_order_total'
        `
  return cy.task('query', sql).then(result => {
    return result[0]
  })
})
Cypress.Commands.add('getXeroAccountCodes', () => {
  const sql = `SELECT option_name as 'name', option_value as 'code'
from wp_options

WHERE option_name  IN ( 'wc_xero_stripe_fee_account' , 'wc_xero_stripe_payment_account' ,'wc_xero_square_pos_cash_payment_account','wc_xero_square_pos_card_payment_account','wc_xero_square_pos_card_fee_account')
`
  return cy.task('query', sql).then(result => {
    const accountCodes = transformObjectArray(result, (account) => {
      return { ...{
          [account.name.replace(/wc_xero_/, '')]: account.code
        }
      }
    })
    // console.log(accountCodes)
    return {
      stripe: {
        payment_account: accountCodes.stripe_payment_account,
        fee_account: accountCodes.stripe_fee_account,
        fee_metaKey: '_stripe_fee'
      },
      square: {
        payment_account: accountCodes.square_pos_card_payment_account,
        fee_account: accountCodes.square_pos_card_fee_account,
        fee_metaKey: 'square_processing_fee_money'
      },
      'square cash': {
        payment_account: accountCodes.square_pos_cash_payment_account
      }
    }

  })
})