export function transformObjectArray(objectArray, transformer) {
  let objects = {}

  objectArray.forEach(element => {
    objects = { ...objects,
      ...transformer(element)
    }
  })
  return objects;
}
/**
 * Compare two objects, assert if they are the same.
 *
 * @param {*} object1
 * @param {*} object2
 */
export function assertObjectsEqual(object1, object2) {
  Object.keys(object1).map(key => {
    let object1_ = JSON.stringify(object1[key]).toString()
    let object2_ = JSON.stringify(object2[key]).toString()
    if (object1_ === object2_) {
      expect(object1_).to.eql(object2_)
    } else {
      Object.keys(object1[key]).map(field => {
        let object1_field = object1[key][field].toString()
        let object2_field = object2[key][field].toString()
        expect(object1_field).contains(object2_field)
      })
    }
  })
}

export function trimBookTitle(title) {
  const NAME_SIZE = 50 - 2
  return title.substring(0, NAME_SIZE).replace(/\&/g, 'and')
}