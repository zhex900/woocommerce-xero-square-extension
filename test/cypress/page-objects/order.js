import faker from 'faker';
import {
    trimBookTitle,
    transformObjectArray
} from '../support/tools'
class Order {
    constructor() {

        this.fields = {
            products: {},
            shipping: {},
            orderId: '',
            billing: {
                firstName: {
                    css: "#billing_first_name",
                    value: faker.name.firstName()
                },
                lastName: {
                    css: "#billing_last_name",
                    value: faker.name.lastName()
                },
                company: {
                    css: '#billing_company',
                    value: faker.company.companyName()
                },
                phone: {
                    css: "#billing_phone",
                    value: '04' + faker.phone.phoneNumber('## ### ###')
                },
                email: {
                    css: "#billing_email",
                    value: "ryde.books@gmail.com" //faker.internet.email().toLocaleLowerCase()
                },
                address: {
                    line1: {
                        css: "#billing_address_1",
                        value: faker.address.streetAddress()
                    },
                    line2: {
                        css: "#billing_address_2",
                        value: ''
                    },
                    city: {
                        css: "#billing_city",
                        value: faker.address.city()
                    },
                    state: {
                        select: {
                            css: "#select2-billing_state-container"
                        },
                        value: ''
                    },
                    postCode: {
                        css: "#billing_postcode",
                        value: faker.address.zipCode('####')
                    }
                },
                toDoListIdField: {
                    css: "#al_filter_item_id"
                }
            }
        };

        this.buttons = {
            shipping: {
                flatRate: {
                    css: "#shipping_method_0_flat_rate1"
                },
                localPickUp: {
                    css: "#shipping_method_0_local_pickup3"
                },
                free: {
                    css: ''
                }
            }
        };
    }
    checkOut() {
        cy.get('.checkout-button').click({
            force: true
        })
        cy.url().should('include', '/checkout')
    }
    placeOrder() {
        cy.get('#place_order').click({
            force: true
        });
        cy.url({
            timeout: 400000
        }).should('include', '/order-pay');
        cy.url().then(url => {
            this.fields.orderId = url.match(/order-pay\/(\d*)/)[1]
            cy.log('Order id: ' + this.fields.orderId)
        })
        return this.fields.orderId
    }
    addRandomProducts(products, numberOfProducts = 2) {
        for (let i = 0; i < numberOfProducts; i++) {
            this.addRandomProduct(products)
        }
    }
    addRandomProduct(products, qty = 1) {
        let randomProduct = faker.random.objectElement(products)
        // console.log(randomProduct)
        cy.visit(`/product/${randomProduct.slug}`)
        // add product to cart
        cy.get('button[name=add-to-cart]').click()
        this.fields.products = { ...this.fields.products,
            ...{
                [randomProduct.id]: randomProduct
            }
        }
        return randomProduct
    }
    stripePay() {
        cy.wait(4000);
        cy.get('iframe.stripe_checkout_app').then($iframe => {
            const body = $iframe.contents().find('#container');
            let stripe = cy.wrap(body);
            stripe
                .find('input')
                .eq(0)
                .click()
                .type('4242424242424242');
            stripe = cy.wrap(body);
            stripe
                .find('input')
                .eq(1)
                .click()
                .clear()
                .type('04')
                .type('30');
            stripe = cy.wrap(body);
            stripe
                .find('input')
                .eq(2)
                .click()
                .type('421')
                .type('{enter}');
        });

        cy.url({
            timeout: 400000
        }).should('contain', '/order-received')

    }

    // square or stripe card payments only
    verifyXeroJournals(orderDetails, orderId, invoiceNumber, paymentMethod, paymentCodes) {
        // get stripe fee
        let sql = `select meta_value as fee_amount from wp_postmeta where post_id=${orderId} AND meta_key="${paymentCodes[paymentMethod].fee_metaKey}"`
        cy.task('query', sql).then(result => {
            let fee_amount = result[0].fee_amount
            cy.task('getXeroManualJournalsById', orderDetails.xeroJournalId).then(lines => {
                // transform journal lines into debit and credit objects
                let journal = transformObjectArray(lines, (line) => {
                    let entryType = 'credit'
                    if (parseFloat(line.LineAmount) > 0) {
                        entryType = 'debit'
                    }
                    return {
                        ...{
                            [entryType]: line
                        }
                    }
                })

                expect(journal.debit.Description).to.eql(`Credit card fees for invoice: ${invoiceNumber}`)
                expect(journal.debit.TaxType).to.eql('INPUT')
                expect(parseFloat(journal.debit.LineAmount)).to.eql(parseFloat(fee_amount))
                expect(journal.debit.AccountCode).to.eql(paymentCodes[paymentMethod].fee_account) //430

                expect(journal.credit.Description).to.eql(`Credit card fees for invoice: ${invoiceNumber}`)
                expect(journal.credit.TaxType).to.eql('OUTPUT')
                expect(parseFloat(journal.credit.LineAmount)).to.eql(parseFloat(fee_amount) * -1)
                expect(journal.credit.AccountCode).to.eql(paymentCodes[paymentMethod].payment_account) // 250
            })
        })
    }
    /**
     *
     // if there is payment verify payment details
     // Check payment type, Square, Cash, Stripe or Invoice (COD)

     /***
      * payment method
      * square or stripe
      * payment_account
      * fee_account
      * fee_amount
      *
      * cash do not have fee_account and no fee_amount
      * only payment or journal
      *
      * invoice do not have payment and no journal
      */
    // let paymentCodes = {
    //     stripe: {
    //         payment_account: '',
    //         fee_account: '',
    //         fee_metaKey: ''
    //     },
    //     square: {
    //         payment_account: '',
    //         fee_account: '',
    //         fee_metaKey: ''
    //     },
    //     square cash: {
    //         payment_account: ''
    //     },
    //
    // }
    /**
     * wc payment methods
     * stripe
     * square_pos_card
     * cod
     * square_pos_cash

     square_pos_card_fee_account: "411"
    square_pos_card_payment_account: "245"
    square_pos_cash_payment_account: "244"
    stripe_fee_account: "430"
    stripe_payment_account: "250" *
        */
    verifyXeroPayment(orderDetails, orderId, invoiceNumber, invoice) {
        let paymentMethod = orderDetails.paymentMethod
        paymentMethod = paymentMethod.replace(/square_pos_card/, 'square')
        paymentMethod = paymentMethod.replace(/square_pos_cash/, 'cash')
        paymentMethod = paymentMethod.replace(/cod/, 'invoice')
        if (paymentMethod !== 'invoice') {
            cy.getXeroAccountCodes().then(paymentCodes => {

                let paymentId = invoice.Payments[0].PaymentID
                cy.task('getXeroPaymentByPaymentId', paymentId).then(payment => {
                    expect(payment.Amount).to.eql(orderDetails.total)
                    expect(payment.Account.Code).to.eql(paymentCodes[paymentMethod].payment_account) //250
                    expect(payment.Account.Name.toLowerCase()).to.eql(`${paymentMethod} clearing account`)
                })

                if (['stripe', 'square_pos_card'].includes(paymentMethod)) { // stripe or square card payments
                    this.verifyXeroJournals(orderDetails, orderId, invoiceNumber, paymentMethod, paymentCodes)
                }
            })
        }
    }

    verifyXeroInvoice(orderId, invoiceNumber) {
        cy.task('getXeroInvoiceByInvoiceNumber', invoiceNumber).then(result => {
            const invoice = result.Invoices[0]
            cy.getOrderLineItems(orderId)
                .then(lineItems => {
                    cy.getOrderDetails(orderId)
                        .then(orderDetails => {
                            console.log(orderDetails)
                            console.log(invoice)

                            this.verifyXeroPayment(orderDetails, orderId, invoiceNumber, invoice)

                            // if paid by Square or Stripe
                            // check credit card fees in manual journal
                            // otherwise there is not be any manual journal

                            // Check total tax
                            expect(Math.round(invoice.TotalTax, 1)).to.eql(Math.round(orderDetails.tax, 1)) // minus rounding or just check the digit not decimal or on
                            // Check total amount
                            expect(invoice.Total).to.eql(orderDetails.total)
                            // Customer details
                            expect(invoice.Contact.FirstName).to.eql(orderDetails.firstName)
                            expect(invoice.Contact.LastName).to.eql(orderDetails.lastName)
                            expect(invoice.Contact.EmailAddress).to.eql(orderDetails.email)
                            // Check line items
                            console.log(lineItems)
                            invoice.LineItems.forEach(item => {
                                console.log(item.ItemCode)
                                let sku = item.ItemCode
                                expect(item.ItemCode).to.eql(lineItems[sku].sku)
                                expect(trimBookTitle(item.Description)).to.eql(trimBookTitle(lineItems[sku].title))
                                expect(parseFloat(item.LineAmount)).to.eql(parseFloat(lineItems[sku].total))
                                expect(parseInt(item.Quantity)).to.eql(parseInt(lineItems[sku].qty))
                                expect(Math.round(parseFloat(item.TaxAmount), 1)).to.eql(Math.round(parseFloat(lineItems[sku].tax), 1))
                            })
                        })
                })
        })


    }
}
export default Order