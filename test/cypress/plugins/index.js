// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
require('dotenv').config()

const MYSQL_CONFIG = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  options: {
    encrypt: false // Use this if you're on Windows Azure
  }
}

const mysqlPool = require('mysql').createPool(MYSQL_CONFIG);
const XeroClient = require('xero-node').AccountingAPIClient;
const XERO_CONFIG = require('./xero-config.json');
const xero = new XeroClient(XERO_CONFIG);
module.exports = (on, config) => {
  on('task', {
    'getXeroProducts': () => {
      return xero.items.get()
    },
    'getXeroInvoiceByInvoiceNumber': (invoiceNumber) => {
      return xero.invoices.get({
        InvoiceNumber: invoiceNumber
      })
    },
    'getXeroPaymentByPaymentId': (paymentId) => {
      return xero.payments.get({
        PaymentID: paymentId
      }).then(result => {
        return result.Payments[0]
      })
    },
    'getXeroManualJournalsById': (id) => {
      return xero.manualJournals.get({
        ManualJournalID: id
      }).then(result => {
        return result.ManualJournals[0].JournalLines
      })
    },
    'deleteAllXeroProducts': (id) => {
      return xero.items.get()
    },
    'query': (sql) => {
      return new Promise((resolve, reject) => {
        mysqlPool.getConnection(function (err, con) {
          if (err) {
            return reject(err);
          } else {
            con.query(sql, function (err, rows) {
              if (err) {
                return reject(err);
              } else {
                con.release(); // releasing connection to pool
                return resolve(rows);
              }
            });
          }
        }); // getConnection
      });
    },
    'log': (message) => {
      if (typeof message === 'string') {
        console.log(`|>> ${message}`)
      } else {
        console.log(`|>>---------------------------------------------------<<|`)
        console.log(message)
        console.log(`|>>---------------------------------------------------<<|`)
      }
      return 'undefined'
    },
  })
}