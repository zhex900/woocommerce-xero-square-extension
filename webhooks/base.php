<?php
require dirname(__FILE__) . '/../../../../wp-blog-header.php';
require_once __DIR__ . '/../../../../vendor/autoload.php';
use Monolog\Logger;

function get_product_id_by_sku($sku)
{
    global $wpdb;
    $results = $wpdb->get_results('select post_id from ' . $wpdb->postmeta . ' where meta_key="_sku" AND meta_value="' . $sku . '"', ARRAY_A);

    square_woo_debug_log('info', "The SQL result of searching for item on woocommerce: product id: " . $results[0]['post_id']);

    if (sizeof($results[0]) > 0) {
        return $results[0]['post_id'];
    } else {
        return false;
    }
}

function square_woo_debug_log($type, $data)
{
    $monolog = new Logger('SQUARE SYNC');
    $client = new Raven_Client(WP_SENTRY_DSN);
    $handler = new Monolog\Handler\RavenHandler($client);
    $handler->setFormatter(new Monolog\Formatter\LineFormatter("%message% %context% %extra%\n"));
    $monolog->pushHandler($handler);

    error_log("[$type] [" . date("Y-m-d H:i:s") . "] " . print_r($data, true) . "\n", 3, '/tmp/logs.log');

    if ($type == 'info') {
        $monolog->info(print_r($data, true));
    } else {
        if ($type == 'error') {
            $monolog->error(print_r($data, true));
        } else {
            $monolog->info(print_r($data, true));
        }
    }
}

/**
 * Respond 200 OK with an optional
 * This is used to return an acknowledgement response indicating that the request has been accepted and then the script can continue processing
 *
 * @param null $text
 */
function respondOK($text = null)
{
// check if fastcgi_finish_request is callable
    if (is_callable('fastcgi_finish_request')) {
        if ($text !== null) {
            echo $text;
        }
        /*
        * http://stackoverflow.com/a/38918192
        * This works in Nginx but the next approach not
        */
        session_write_close();
        fastcgi_finish_request();

        return;
    }

    ignore_user_abort(true);

    ob_start();

    if ($text !== null) {
        echo $text;
    }

    $serverProtocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
    header($serverProtocol . ' 200 OK');
// Disable compression (in case content length is compressed).
    header('Content-Encoding: none');
    header('Content-Length: ' . ob_get_length());

// Close the connection.
    header('Connection: close');

    ob_end_flush();
    ob_flush();
    flush();
}
