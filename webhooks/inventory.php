<?php
require_once __DIR__ . '/base.php';

square_woo_debug_log('info', "Square sync callback page called.");

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    square_woo_debug_log('info', "Callback page called via get request.");
}

$post_data = json_decode(file_get_contents("php://input"));
respondOK();

if (!$post_data) {
    square_woo_debug_log('info', "Callback page called via POST request. but there is no post data.");
    echo die('Callback request working with no post data');
}

square_woo_debug_log('info', "Callback page called via POST with post data (json format) " . print_r($post_data, true) . $HTTP_RAW_POST_DATA);

// get the date time range for the last 24 hours.
date_default_timezone_set('Australia/Sydney');

//Check if this request is "Refund Request"
if (isset($post_data->type) && $post_data->event_type == "TEST_NOTIFICATION") {
    square_woo_debug_log('info', "This is a manual call from Square test notifications. ");
//    $begin_time = "2019-06-10T00:00:00Z"; //
//    $end_time = "2019-06-19T00:00:00Z"; //
}elseif (isset($post_data->type) && $post_data->type == "inventory.count.updated") {
    square_woo_debug_log('info', "inventory.count.updated ");
    process_inventory_update($post_data->data);
    return;
}else{
    return;
}

function process_inventory_update($data)
{
    square_woo_debug_log('info', $data->object->inventory_counts);

    foreach ($data->object->inventory_counts as $inventory_count){
        square_woo_debug_log('info', "update inventory catalog_object_id ". $inventory_count->catalog_object_id);
        updateWooInventoryBySquareCatalogId($inventory_count);
    }
}

function updateWooInventoryBySquareCatalogId($inventory_count)
{
    // update only if the inventory is in the same location as the woo location
    $square = new WC_XR_Request_Square_Get_Inventory_Changes("");
    if( $inventory_count->location_id !== $square->get_location_id() ){
        return;
    }

    // get Square inventory update details.
    $changes = get_inventory_changes($inventory_count);

    $is_stock_recount = isInventoryStockRecount($inventory_count,$changes);
    square_woo_debug_log('info', "is_stock_recount $is_stock_recount");
    if( $is_stock_recount ){
        // find the product sku code
        $sku = get_inventory_sku_by_catalog_id($inventory_count->catalog_object_id);
        square_woo_debug_log('info', "update inventory sku " . $sku);
        update_product_inventory($sku,$inventory_count->quantity);
    }
}

function update_product_inventory($sku, $quantity){
    $product_id = get_product_id_by_sku($sku);
    // update woo inventory
    // Get an instance of the WC_Product object
    $product = new WC_Product( $product_id );
    $product->set_manage_stock(true);
    $product->set_stock_quantity($quantity);
    if($quantity>0) {
        $product->set_stock_status('instock');
    }else{
        $product->set_stock_status('outofstock');
    }
    $product->save();
}

function get_inventory_sku_by_catalog_id($catalog_object_id){
    $request = new WC_XR_Request_Square_Get_Catalog_Object($catalog_object_id);
    $error = $request->do_request();
    $response = $request->get_response_json();
    square_woo_debug_log('info', $response);
    if (empty($response) || property_exists($response, 'errors')) {
        // some kind of an error happened
        square_woo_debug_log('error', "The response of get_inventory_sku_by_catalog_id details curl request " . $error);
        return null;
    } else {
        return $response->object->item_variation_data->sku;
    }
}

function get_inventory_changes($inventory_count)
{
    $request = new WC_XR_Request_Square_Get_Inventory_Changes($inventory_count->catalog_object_id);
    $error = $request->do_request();
    $response = $request->get_response_json();
    square_woo_debug_log('info', $response);
    if (empty($response) || property_exists($response, 'errors')) {
        // some kind of an error happened
        square_woo_debug_log('error', "The response of get_inventory_changes details curl request " . $error);
        return null;
    } else {
       return $response->changes;
    }
}

function isInventoryStockRecount($inventory_count,$changes){
    foreach ($changes as $change){
        if( $change->type === "PHYSICAL_COUNT" && $change->physical_count->state === "IN_STOCK" && $change->physical_count->created_at===$inventory_count->calculated_at ){
            square_woo_debug_log('info', "isInventoryStockRecount ");
            square_woo_debug_log('info', $change);
            return true;
        }
    }
    return false;
}
