<?php
require_once __DIR__ . '/base.php';
const SQUARE_QUEUE = '/tmp/square_queue.log';
square_woo_debug_log( 'info', "Square sync callback page called." );

if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
	square_woo_debug_log( 'info', "Callback page called via get request." );
}

$post_data = json_decode( file_get_contents( "php://input" ) );
respondOK();

if ( ! $post_data ) {
	square_woo_debug_log( 'info', "Callback page called via POST request. but there is no post data." );
	echo die( 'Callback request working with no post data' );
}

square_woo_debug_log( 'info',
	"Callback page called via POST with post data (json format) " . print_r( $post_data, true ) . $HTTP_RAW_POST_DATA );

// get the date time range for the last 24 hours.
date_default_timezone_set( 'Australia/Sydney' );

//Check if this request is "Refund Request"
if ( isset( $post_data->event_type ) && $post_data->event_type == "TEST_NOTIFICATION" ) {
	square_woo_debug_log( 'info', "This is a manual call from Square test notifications. " );
	$begin_time             = "2019-09-28T00:00:00Z"; //
	$end_time               = "2019-09-29T00:00:00Z"; //
	$post_data->location_id = 'CTP81RKV1PC8J'; //

	square_woo_debug_log( 'info', "Callback page called via POST with post data (json format) " . print_r( $post_data,
			true ) . $HTTP_RAW_POST_DATA );
	process_payments( $post_data );
} elseif ( isset( $post_data->event_type ) && $post_data->event_type == "PAYMENT_UPDATED" ) {
	square_woo_debug_log( 'info', "PAYMENT_UPDATED" );

	process_payments( $post_data );
} else {
	return;
}

function process_payments( $post_data ) {
//    $paymentID = $post_data->entity_id;
    $end_time = date('Y-m-d\TH:i:s\Z');
    $begin_time = date('Y-m-d\TH:i:s\Z', strtotime($end_time . ' - 24 hours'));

//	$begin_time = "2020-01-19T00:00:00Z"; //
//	$end_time   = "2020-01-20T00:00:00Z"; //
////
//	$post_data->location_id = 'CTP81RKV1PC8J'; // staging

	$payment_obj = get_payments( $begin_time, $end_time, $post_data->location_id );

	$refunds = [];
	$i       = 1;
	square_woo_debug_log( 'info', 'Number of payments: ' . count( $payment_obj ) );
	foreach ( $payment_obj as $payment ) {
		square_woo_debug_log( 'info', 'Payment: ' . $i ++ . ' ' . $payment->id );
		if ( $payment ) {
			//Check if this request is "Refund Request"
			try {
				if ( count( $payment->refunds ) ) {
					$refunds[] = $payment;
				} else {
					$ran = rand( 15000, 25000 ); // 15 seconds
					usleep( $ran );
					square_woo_debug_log( 'sleep', "sleep for " . $ran );

					if ( ! file_exists( SQUARE_QUEUE ) || ( file_exists( SQUARE_QUEUE ) && ! exec( 'grep ' . escapeshellarg( $payment->id ) . ' /tmp/square_queue.log' ) ) ) {
						global $wpdb;
						$checkif_order_not_exist = $wpdb->get_results( "SELECT * FROM `" . $wpdb->postmeta . "` WHERE meta_key='" . $wpdb->escape( 'square_payment_id' ) . "' AND meta_value='" . $wpdb->escape( $payment->id ) . "'" );

						if ( empty( $checkif_order_not_exist ) ) { //} && ($paymentID == "TEST1" || $payment->id == $paymentID)) {
							create_order( $payment, $post_data->location_id );
						}
					}
				}
			} catch ( Exception $e ) {
				square_woo_debug_log( 'error', $e );
			}
		}
	}
	foreach ( $refunds as $refund ) {
		process_refund( $refund );
	}
}

function get_transaction_id( $payment_url ) {
	preg_match( '/.*\/(.*)$/', $payment_url, $output_array );
	square_woo_debug_log( 'info', "Transaction id: " . $output_array[1] );

	return $output_array[1];
}

/**
 *
 * Find the matching WooCommerce customer from the square payment.
 *
 *
 * @param $payment
 *
 * @return bool|WP_User
 */
function get_user( $payment, $location_id ) {
	// default woocommerce customer
	$square_user = get_user_by( 'login', 'square_user' );
	square_woo_debug_log( 'info', 'default square_user :' . $square_user->ID );
	$square_customer = get_payment_customer( $payment, $location_id );

	if ( is_null( $square_customer ) ) {
		return $square_user; //default customer square_user
	} else {
		square_woo_debug_log( 'info', 'get woo customer by square customer' );
		square_woo_debug_log( 'info', 'woo_user id:' . $square_customer->reference_id );
		// sync square customer with woo
		$user = WC_XR_Sync_Customer_Square::get_woo_customer_by_square_customer( $square_customer );

		return $user;
	}
}

function get_wc_user_address( WP_User $customer ) {
	$address_type    = 'billing';
	$billing_address = array(
		$address_type . "_address_1",
		$address_type . "_address_2",
		$address_type . "_city",
		$address_type . "_state",
		$address_type . "_postcode",
		'first_name',
		'last_name',
		$address_type . '_email',
		$address_type . '_phone'
	);

	foreach ( $billing_address as $line ) {
		$billing_address[ $line ] = get_user_meta( $customer->ID, $line, true );
	}

	return $billing_address;
}

/**
 *
 * Find the customer who made the payment. If no customer is recorded
 * null is returned.
 *
 * The square customer id is stored in the transactions. First make
 * a request to get the transactions. From the transactions get the
 * customer id. After that, get the customer details from Square.
 *
 * Of course, not all payments have the customer record.
 *
 * @param object $payment
 *
 * @return null|object $customer
 */
function get_payment_customer( $payment, $location_id ) {
	$transaction_id = get_transaction_id( $payment->payment_url );
	$request        = new WC_XR_Request_Square_Get_Transactions( $transaction_id, $location_id );
	$error          = $request->do_request();
	$response       = $request->get_response_json();
	square_woo_debug_log( 'info', $response );
	if ( empty( $response ) || property_exists( $response, 'errors' ) ) {
		// some kind of an error happened
		square_woo_debug_log( 'error', "The response of transaction details curl request " . $error );

		return null;
	} else {
		square_woo_debug_log( 'info', "The response of transaction details curl request " );

		if ( property_exists( $response->transaction->tenders[0], 'customer_id' ) ) {
			$customer_id = $response->transaction->tenders[0]->customer_id;
			square_woo_debug_log( 'info', "The square customer id: " . $customer_id );
			$request  = new WC_XR_Request_Square_Get_Customer( $customer_id );
			$error    = $request->do_request();
			$response = $request->get_response_json();

			square_woo_debug_log( 'info', $response );
			if ( empty( $response )
			     || property_exists( $response, 'errors' )
			     || ! is_square_customer_valid( $response->customer ) ) {
				square_woo_debug_log( 'error',
					"The response of WC_XR_Request_Square_Get_Customer details curl request " . $error );

				return null;
			} else {
				square_woo_debug_log( 'info', "The response of WC_XR_Request_Square_Get_Customer details curl request " );

				return $response->customer;
			}
		} else {
			return null;
		}
	}
}

function is_square_customer_valid( $customer ) {
	return property_exists( $customer, 'given_name' ) && $customer->given_name != '' &&
	       property_exists( $customer, 'family_name' ) && $customer->family_name != '' &&
	       property_exists( $customer, 'email_address' ) && $customer->email_address != '';
}

function get_payments( $begin_time, $end_time, $location_id ) {
	square_woo_debug_log( 'info', "Get all square payments from $begin_time to $end_time, location: $location_id" );

	$request  = new WC_XR_Request_Square_Get_Payments( $begin_time, $end_time, $location_id );
	$err      = $request->do_request();
	$response = $request->get_response_json();

	square_woo_debug_log( 'info', "All transaction response" . print_r( $response ) );
	if ( empty( $response ) || property_exists( $response, 'errors' ) ) {
		// some kind of an error happened
		square_woo_debug_log( 'error', "The response of payment details curl request " . $err );

		return [];
	} else {
		square_woo_debug_log( 'info', "The response of payment details curl request " );

		return $response;
	}
}

function add_address_to_order( WC_Order $order, WP_User $customer ) {
	$address = get_wc_user_address( $customer );
	square_woo_debug_log( 'info', 'add customer address to order' );
	square_woo_debug_log( 'info', print_r( $address, true ) );

	$order->set_billing_address_1( $address['billing_address_1'] );
	$order->set_billing_address_2( $address['billing_address_2'] );
	$order->set_billing_city( $address['billing_city'] );
	$order->set_billing_postcode( $address['billing_postcode'] );
	$order->set_billing_state( $address['billing_state'] );
	$order->set_billing_first_name( $address['first_name'] );
	$order->set_billing_last_name( $address['last_name'] );
	$order->set_billing_email( $address['billing_email'] );
	$order->set_billing_phone( $address['billing_phone'] );

	$order->set_shipping_address_1( $address['billing_address_1'] );
	$order->set_shipping_address_2( $address['billing_address_2'] );
	$order->set_shipping_city( $address['billing_city'] );
	$order->set_shipping_postcode( $address['billing_postcode'] );
	$order->set_shipping_state( $address['billing_state'] );
	$order->set_shipping_first_name( $address['first_name'] );
	$order->set_shipping_last_name( $address['last_name'] );
	square_woo_debug_log( 'info', print_r( $order->get_address( 'billing' ), true ) );
}

function getLocationNameById( $location_id ) {
	$request  = new WC_XR_Request_Square_Get_Location_Details();
	$error    = $request->do_request();
	$response = $request->get_response_json();

	square_woo_debug_log( 'info', $response->locations );
	foreach ( $response->locations as $location ) {
		if ( $location->id == $location_id ) {
			return $location->name;
		}
	}

	return '';
}

function filter_woocommerce_can_reduce_order_stock( $true, $instance ) {
	return false;
}

/**
 * create order in woo
 *
 * @param $payment
 */
function create_order( $payment, $location_id ) {
	square_woo_debug_log( 'info', "Creating new order." );
	file_put_contents( SQUARE_QUEUE, $payment->id . PHP_EOL, FILE_APPEND );

	$user = get_user( $payment, $location_id );
	$args = [
		'customer_id'   => $user->ID,
		'created_via'   => 'Square',
		'customer_note' => 'Location: ' . getLocationNameById( $location_id ) . ' ' . $location_id
	];

	// Do not reduce stock if the linked Square location is not the same
	// as the webhook location.
	$request = new WC_XR_Request_Square_Get_Location_Details();

	if ( $request->get_location_id() != $location_id ) {
		add_filter( 'woocommerce_can_reduce_order_stock', 'filter_woocommerce_can_reduce_order_stock', 10, 2 );
	}

	$order = wc_create_order( $args );
	// add customer billing and shipping address to the order.
	add_address_to_order( $order, $user );
	$order_id = $order->get_order_number();
	add_product( $payment, $order );
	add_payment( $payment, $order );

	$date = get_payment_date( $payment );
	global $wpdb;
	$sql = "UPDATE `wp_posts` SET `ID` = '" . $order_id . "', `post_date_gmt` = '" . $date . "', `post_date` = '" . $date . "' WHERE `ID` = '" . $order_id . "'";
	$rez = $wpdb->query( $sql );

	exec( 'sed -i /' . escapeshellarg( $payment->id ) . '/d /tmp/square_queue.log' );
}

function add_rounding( $payment, WC_Order &$order ) {

	square_woo_debug_log( 'info', print_r( $payment, true ) );

	$rounding = round( $payment->total_collected_money->amount / 100 - $order->get_total(), 2 );
	square_woo_debug_log( 'info',
		"Check rounding " . $rounding . ' ' . $payment->total_collected_money->amount / 100 . ' ' . $order->get_total() );
	if ( $rounding != 0 ) {
		$product_id = get_product_id_by_sku( 'rounding-adjustment' );
		$order->add_product( wc_get_product( $product_id ), 1, [
			'subtotal' => $rounding, //$custom_price_for_this_order, // e.g. 32.95
			'total'    => $rounding, //$custom_price_for_this_order, // e.g. 32.95
		] );

		square_woo_debug_log( 'info', "Add rounding adjustment  " . $rounding );
		$order->calculate_totals();
		square_woo_debug_log( 'info',
			"rounding result: payment total:" . $payment->total_collected_money->amount / 100 . " order total: " . $order->get_total() );

		if ( $payment->total_collected_money->amount / 100 !== $order->get_total() ) {
			square_woo_debug_log( 'error', 'ORDER AMOUNT DO NOT MATCH' );
			die();
		}
		$order->save();
	}
}

function get_payment_date( $payment ) {
	$payment_date = $payment->created_at;
	$payment_date = str_replace( "Z", "", $payment_date );
	$payment_date = explode( "T", $payment_date );

	return $payment_date[0] . " " . $payment_date[1];
}

function process_refund( $payment ) {
	square_woo_debug_log( 'info', "Creating new refund. Payment ID: " . $payment->id );
	global $wpdb;
	$results = $wpdb->get_results( "select post_id, meta_key from $wpdb->postmeta where meta_value = '$payment->id'",
		ARRAY_A );
	if ( count( $results ) ) {
		$order_id   = $results[0]['post_id'];
		$created_at = get_post_meta( $order_id, "refund_created_at", true );

		if ( $created_at != $payment->refunds[0]->created_at and ! empty( $payment->refunds[0]->created_at ) ) { // Avoid duplicate insert in case we refund from woo commerce which will fire payment update webhooks, so we need do nothing in this case to avoid dubplicate insertion
			$order = new WC_Order( $order_id );
			square_woo_debug_log( 'info', "Creating WC refund for order# " . $order_id );
			$square_refund_amount = - 1 * $payment->refunds[0]->refunded_money->amount / 100;
			square_woo_debug_log( 'info', "Square payment object: " . print_r( $payment, true ) );
			square_woo_debug_log( 'info',
				"Square payment refund amount: " . print_r( - 1 * $payment->refunds[0]->refunded_money->amount / 100,
					true ) );
			square_woo_debug_log( 'info', "WC remaining refund amount object: " . $order->get_remaining_refund_amount() );
			if ( $square_refund_amount > $order->get_remaining_refund_amount() ) {
				square_woo_debug_log( 'info', "Refund cannot be greater than the order amount" );

				return;
			}
			$refund_obj = wc_create_refund( [
				'order_id' => $order_id,
				'amount'   => - 1 * $payment->refunds[0]->refunded_money->amount / 100,
				'reason'   => $payment->refunds[0]->reason
			] );
			if ( is_wp_error( $refund_obj ) ) {
				square_woo_debug_log( 'info', "wc_refund-> " . print_r( $refund_obj, true ) );

				return;
			}

			$refund_obj->set_refunded_by( get_user_by( 'login', 'square_user' )->ID ); // get square pos user
			$refund_obj->save();
			square_woo_debug_log( 'info', "wc_refund:" . print_r( $refund_obj->get_id(), true ) );

			square_woo_debug_log( 'info', "wc_refund:" . print_r( $refund_obj->get_amount(), true ) );


			$customer_id = $order->get_customer_id();
			square_woo_debug_log( 'info', "wc_refund customer id :" . $customer_id );

			wp_update_post( [ 'ID' => $refund_obj->get_id(), 'post_author' => $customer_id ] );
			update_post_meta( $order_id, "refund_created_at", $payment->refunds[0]->created_at );
			//increase stock after refund.
			foreach ( $payment->itemizations as $item ) {
				$variation_id = $item->item_detail->item_variation_id;
				$args         = [
					'post_type'  => [ 'product', 'product_variation' ],
					'meta_query' => [ [ 'key' => 'variation_square_id', 'value' => $variation_id ] ],
					'fields'     => 'ids',
				];
				$vid_query    = new WP_Query( $args );
				$vid_ids      = $vid_query->posts;
				if ( $vid_ids ) {
					$product_id = $vid_ids[0];
					$product    = get_product( $product_id );
					if ( $product && $product->managing_stock() ) {
						$old_stock = $product->get_stock_quantity();
						$new_stock = wc_update_product_stock( $product, $item->quantity, 'increase' );

						do_action( 'woocommerce_restock_refunded_item', $product->get_id(), $old_stock, $new_stock,
							$order, $product );
					}

				}
			}
		}
	} else {
		square_woo_debug_log( 'error', "Refund order is not yet created in WC" );
	}
}

/**
 *
 * This is a workaround solution.
 *
 * Normally, the Square payment object have the processing_fee_money.
 * But for EFTPOS processing_fee_money amount is zero. This might be a bug.
 * EFTPOS does incur a 1.9% fee.
 *
 * To solve this, this function manually calculate the Square processing fee.
 *
 * @param $payment
 *
 * @return float|int
 *
 */
function calculate_square_processing_fee( $payment ) {
	$rate = 1.9;

	if ( floatval( $payment->processing_fee_money->amount ) == 0 ) {
		return round( $payment->total_collected_money->amount * $rate / 10000, 2 );
	} else {
		return floatval( $payment->processing_fee_money->amount ) / 100 * - 1;
	}
}

function add_payment( $payment, WC_Order &$order ) {

	$order_id = $order->get_order_number();
	add_post_meta( $order_id, 'square_payment_id', $payment->id );
	add_post_meta( $order_id, 'square_receipt_url', $payment->receipt_url );
	add_post_meta( $order_id, 'square_tender', json_encode( $payment->tender ) );

	add_post_meta( $order_id, 'square_device', json_encode( $payment->device ) );
	add_post_meta( $order_id, 'square_payment_url', json_encode( $payment->payment_url ) );
	$card_details = '';

	if ( $payment->tender[0]->type == 'CREDIT_CARD' ) {
		$order->set_payment_method( 'square_pos_card' );
		$order->set_payment_method_title( 'Square POS Card' );
		add_post_meta( $order_id, 'square_processing_fee_money', calculate_square_processing_fee( $payment ) );
		$card_details = ". Card: " . $payment->tender[0]->card_brand . ", ending:  " . $payment->tender[0]->pan_suffix . ".";

	} else {
		$order->set_payment_method( 'square_pos_cash' );
		$order->set_payment_method_title( 'Square POS Cash' );

	}

	// created_at
	$transaction_id = get_transaction_id( $payment->payment_url );
	$order->add_order_note( sprintf( __( 'Square POS %s payment. %s', 'wc-square' ), $payment->tender[0]->name,
		$card_details ) );
	$order->add_order_note( sprintf( __( 'Square transaction id: %s.', 'wc-square' ),
		"<a href=\"{$payment->payment_url}\">{$transaction_id}</a>" ) );
	$order->add_order_note( sprintf( __( 'Square payment receipt id: %s. ', 'wc-square' ),
		"<a href=\"{$payment->receipt_url}\">{$payment->id}</a>" ) );
	$order->calculate_totals();

	if ( ($payment->total_collected_money->amount / 100) - $order->get_total() > 0.01 ) {
		square_woo_debug_log( 'error', 'ORDER AMOUNT DO NOT MATCH. Square ( ' . $payment->total_collected_money->amount / 100 . ') Woo (' . $order->get_total() . ')' );
		die();
	}

//    add_rounding($payment, $order);

	$order->payment_complete( $transaction_id );
	$order->update_status( 'completed' );
}


// default tax rate is 10
// this is the rate is use to calculate the GST from total price that is GST inclusive price
function get_product_tax_rate( WC_Product $product ) {

	$tax_rates = WC_Tax::get_rates( $product->get_tax_class() );
	$tax_rate  = 10 / 100; //default GST is 10%
	square_woo_debug_log( 'info', "product class tax rate:" . print_r( $tax_rates, true ) );
	if ( sizeof( $tax_rates ) > 0 ) {
		$tax_rate = array_shift( $tax_rates )['rate'] / 100;
		square_woo_debug_log( 'info', "product tax rate:" . print_r( $tax_rate, true ) );
	}

	return round( ( 1 + $tax_rate ) / $tax_rate, 2 );
}

function add_product( $payment, WC_Order &$order ) {

	$customAmountTotal = 0;
	foreach ( $payment->itemizations as $item ) {

		square_woo_debug_log( 'info', "Square item details : " . json_encode( $item ) );
		if ( $item->name === "Custom Amount" ) {
			$customAmountTotal += $item->total_money->amount / 100 * $item->quantity;
			continue;
		}

		$item_id = $item->item_detail->item_id;
		$sku     = $item->item_detail->sku;
		square_woo_debug_log( 'info', "Square item item id : " . $item_id . ' sku: ' . $sku );

		$product_id = get_product_id_by_sku( $sku );

		// do something if the meta-key-value-pair exists in another post
		if ( $product_id !== false ) {
			$product = wc_get_product( $product_id );

			$tax_rate = get_product_tax_rate( $product );

			square_woo_debug_log( 'info', "tax rate:" . print_r( $tax_rate, true ) );

			$total_tax_inclusive                = $item->total_money->amount / 100;
			$tax                                = round( $total_tax_inclusive / $tax_rate, 2 ); // calculate the GST manually.
			$square_product_price_tax_exclusive = $total_tax_inclusive - $tax;

			square_woo_debug_log( 'info', "product total_tax_inclusive:" . $total_tax_inclusive );

			// use Square price
			$order->add_product( $product, $item->quantity, [
				'subtotal' => $square_product_price_tax_exclusive, //$custom_price_for_this_order, // e.g. 32.95
				'total'    => $square_product_price_tax_exclusive, //$custom_price_for_this_order, // e.g. 32.95
			] );

			square_woo_debug_log( 'info', "payment item amount:" . $square_product_price_tax_exclusive );

		} else {
			square_woo_debug_log( 'error', "Product not found on woocommerce. This should not happen" );
		}
	}

	if ( $customAmountTotal > 0 ) {
		$sku = 'custom-amounts';
		$product_id = get_product_id_by_sku( $sku );
		$product = wc_get_product( $product_id );
		$tax_rate = get_product_tax_rate( $product );

		$total_tax_inclusive                = $customAmountTotal;
		$tax                                = round( $total_tax_inclusive / $tax_rate, 2 ); // calculate the GST manually.
		$square_product_price_tax_exclusive = $total_tax_inclusive - $tax;
		$product->set_regular_price( $total_tax_inclusive );
		$product->save();
		square_woo_debug_log( 'info', "custom amount total_tax_inclusive:" . $total_tax_inclusive );

		// use Square price
		$order->add_product( $product, 1, [
			'subtotal' => $square_product_price_tax_exclusive, //$custom_price_for_this_order, // e.g. 32.95
			'total'    => $square_product_price_tax_exclusive, //$custom_price_for_this_order, // e.g. 32.95
		] );

		square_woo_debug_log( 'info', "payment item amount:" . $square_product_price_tax_exclusive );


	}
}

